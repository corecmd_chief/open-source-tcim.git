package com.corecmd.tcloud.tcimserver;

import com.corecmd.tcloud.tcimserver.utils.ImCrossServerPushUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
class TcimserverApplicationTests {
    @Autowired
    private StringRedisTemplate redisTemplate;


    @Test
    void contextLoads() {

    }
    @Test
    void testRedis(){
        // 获取所有的key
        Set<String> keys = redisTemplate.keys("tcImClient_2_*");
        if (null != keys){
            // 批量获取数据
            List<String> values = redisTemplate.opsForValue().multiGet(keys);
            log.info(values.toString());
        }
    }

    @Test
    void testHttpPost(){
        try {
            Map<String,String> params = new HashMap<String,String>();
            params.put("senderId","5");
            params.put("receiverId","3");
            params.put("msgContent","测试消息");
            params.put("msgType","8");
            params.put("msgContentType","1");
            params.put("sendTimestamp","fasdfasd");
            Map<String,String> headers = new HashMap<String,String>();
            headers.put("authToken","6895903d-454b-4ef6-b6a8-00fc3859669f");
            String httpUrl = "http://192.168.1.4:18900/rest/imServer/pushMessage";
            boolean crossPushResult = ImCrossServerPushUtil.crossServerPush(httpUrl,params,headers);
            log.info("跨服务器推送好友添加请求消息到客户端：{} {}","3",crossPushResult?"成功":"失败");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
