create table im_chat_group
(
    id            varchar(40)  not null comment '群id'
        primary key,
    groupName     varchar(255) null comment '群名称',
    createTime    datetime     null comment '创建时间',
    updateTime    datetime     null comment '更新时间',
    avatarUrl     varchar(255) null comment '群头像url',
    description   varchar(255) null comment '群介绍',
    tags          varchar(255) null comment '群标签',
    status        char         null comment '群状态 0：可用  1：禁用',
    chatForbidden char         null comment '是否全员禁言 0：是  1：否',
    creatorId     varchar(40)  null comment '创建人id',
    ownerId       int          null comment '群主'
)
    comment 'im群组';

create table im_chat_group_add_request
(
    id         int         not null comment '主键id'
        primary key,
    groupId    varchar(40) null comment '群组id',
    senderId   int         null comment '申请人id',
    remark     varchar(40) null comment '备注信息',
    status     char        null comment '申请状态 0：已同意 1：未同意  2：已拒绝',
    createTime datetime    null comment '创建时间',
    updateTime datetime    null comment '更新时间'
)
    comment '加群请求表';

create table im_chat_group_memeber
(
    id          varchar(40)  not null comment '主键id'
        primary key,
    chatGroupId varchar(40)  null comment '群组id',
    memberId    int          null comment '成员id',
    createTime  datetime     null comment '创建时间',
    updateTime  datetime     null comment '更新时间',
    status      char         null comment '状态： 0：已加入 1：审核中  2：已拒绝',
    description varchar(255) null comment '备注信息',
    groupRoleId varchar(40)  null comment '群角色Id'
)
    comment 'im群组和成员绑定关系表';

create table im_chat_session
(
    id           varchar(40) null comment '会话id',
    ownerId      int         null comment '所属人',
    receiverId   varchar(40) null comment '会话消息接收人方，好友 或者群等',
    createTime   datetime    null comment '创建时间',
    updateTime   datetime    null comment '更新时间',
    type         char        null comment '会话类型，单聊、群聊、系统消息等',
    status       char        null comment '会话状态 0：可用  1：禁用',
    topStick     char        null comment '是否置顶 0：置顶  1：不置顶',
    enableNotify char        null comment '是否启用消息通知，即消息免打扰 0：开启  1：关闭'
)
    comment '聊天会话表';

create table im_friend
(
    id             varchar(40)  not null comment '主键id'
        primary key,
    nickName       varchar(255) null comment '好友昵称',
    remarkNickName varchar(40)  null comment '备注昵称',
    friendId       int          null comment '好友id',
    ownerId        int          null comment '所属人id',
    createTime     datetime     null comment '创建时间',
    updateTime     datetime     null comment '更新时间',
    relationStatus char         null comment '好友关系状态 0：正常 1：未同意添加  2：黑名单',
    groupId        varchar(40)  null comment '所属分组',
    tags           varchar(255) null comment '好友标签',
    description    varchar(255) null comment '好友备注'
)
    comment 'im好友表';

create table im_friend_add_request
(
    id         varchar(40)  not null comment '主键id'
        primary key,
    friendId   int          null comment '好友id',
    senderId   int          null comment '申请人id',
    ownerId    int          null comment '所属人id',
    remark     varchar(255) null comment '备注信息',
    status     char         null comment '申请状态 0：已同意 1：未同意  2：已拒绝',
    createTime datetime     null comment '创建时间',
    updateTime datetime     null comment '更新时间'
)
    comment '好友添加请求表';

create table im_friend_group
(
    id           varchar(40)  not null comment '好友分组id'
        primary key,
    name         varchar(255) null comment '分组名称',
    createTime   datetime     null comment '创建时间',
    updateTime   datetime     null comment '更新时间',
    ownerId      int          null comment '所属人',
    status       char         null comment '状态 0：可用 1：禁用',
    description  varchar(255) null comment '描述',
    sortNum      int          null comment '排序号',
    tags         varchar(40)  null comment '标签',
    parentId     varchar(40)  null comment '父级分组id',
    defaultGroup char         null comment '是否默认分组 0：是 1：否'
)
    comment 'im好友分组表';

create table im_message
(
    id            varchar(40) not null comment '消息表'
        primary key,
    chatSessionId varchar(40) null comment '所属会话',
    senderId      int         null comment '发送人id',
    receiverId    int         null comment '接收人',
    createTime    datetime    null comment '创建时间',
    updateTime    datetime    null comment '更新时间',
    status        char        null comment '消息状态 0：启用  1：禁用',
    readFlag      char        null comment '阅读状态 0：已读  1：未读',
    messageType   char        null comment '消息类型 1：上线通知 2：下线通知  3：系统强制踢出 4：窗口抖动 5：单聊 6: 群聊 7:系统通知',
    contentType   char        null comment '消息内容类型 1：文本 2：图像  3：音频 4：视频 5：文件等',
    msgContent    text        null comment '消息内容'
)
    comment 'im消息表';

create table userinfo
(
    id         int auto_increment comment '用户id,平台分配的数字账号,自动递增'
        primary key,
    userName   varchar(255) null comment '用户昵称',
    password   varchar(255) null comment '用户密码',
    status     char         null comment '用户状态 0：可用 1：禁用',
    email      varchar(255) null comment '邮箱',
    createTime datetime     null comment '创建时间',
    updateTime datetime     null comment '更新时间',
    summary    varchar(255) null comment '个人介绍',
    tags       varchar(255) null comment '标签',
    signature  varchar(255) null comment '个性签名',
    gender     char         null comment '性别 1：男  0：女 2：未知',
    country    varchar(255) null comment '国家',
    avatarUrl  varchar(255) null comment '头像',
    province   varchar(255) null comment '省份',
    city       varchar(255) null comment '城市',
    userType   char         null comment '0：平台注册  1：第三方注册'
)
    comment '用户信息表';

