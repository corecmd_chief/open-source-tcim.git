package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 用户信息
 **/
@Data
public class UserInfoDO implements Serializable {
    //平台分配的数字账号
    private int id;
    private String userName;
    @JsonIgnore
    private String password;
    private char status;
    //电子邮箱
    private String email;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //个签
    private String signature;
    //标签  多个标签之间用 ; 分割
    private String tags;
    //个人简介
    private String summary;
    //性别 1：男  0：女 2：未知
    private String gender;
    //国家
    private String country;
    //头像
    private String avatarUrl;
    //省份
    private String province;
    //城市
    private String city;
    //0：平台注册  1：第三方注册
    private char userType;
}
