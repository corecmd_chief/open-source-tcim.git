package com.corecmd.tcloud.tcimserver.im.starter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
@Component
@Order(1)
public class StartupServer implements ApplicationRunner {
    @Autowired
    private ImServer imServer;
    @Value("${imserver.port}")
    private int imServerPort;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("启动ImServer");
        new Thread(new Runnable() {
            @Override
            public void run() {
                imServer.start(imServerPort);
            }
        }).start();
    }
}
