package com.corecmd.tcloud.tcimserver.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.Charsets;

import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description :
 **/
@Slf4j
public class ImCrossServerPushUtil {

    /**
     * 跨服务器推送im消息
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static boolean crossServerPush(String url, Map<String,String> params, Map<String,String> headers) throws Exception{
        boolean isSuccess = false;
        try {
            String pushResult = HttpClientUtils.httpFormPost(url,params,headers, Charsets.toCharset("UTF-8"));
            log.info("跨服务器消息推送结果："+pushResult);
            JSONObject jsonResult = JSONObject.parseObject(pushResult);
            if ("200".equals(jsonResult.getString("statusCode"))){
                isSuccess = true;
            }
        } catch (Exception e) {
            log.info("跨服务器消息推送异常",e);
        }
        return isSuccess;
    }
}
