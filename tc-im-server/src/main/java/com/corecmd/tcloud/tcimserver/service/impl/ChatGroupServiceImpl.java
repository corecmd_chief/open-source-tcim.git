package com.corecmd.tcloud.tcimserver.service.impl;

import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.mapper.IChatGroupMapper;
import com.corecmd.tcloud.tcimserver.service.IChatGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Slf4j
@Service
public class ChatGroupServiceImpl implements IChatGroupService {
    @Autowired
    private IChatGroupMapper chatGroupMapper;

    @Override
    public List<UserInfoDTO> getMembers(String groupId, Map<String, Object> query) {
        query.put("groupId",groupId);
        return chatGroupMapper.getMembers(query);
    }
}
