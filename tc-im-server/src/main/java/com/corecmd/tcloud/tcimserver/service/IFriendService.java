package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimserver.domain.ImFriendAddRequestDTO;
import com.corecmd.tcloud.tcimserver.domain.ImFriendDTO;
import com.corecmd.tcloud.tcimserver.domain.ImFriendGroupDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.enums.FriendAddReqSuggestionEnum;

import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description :
 **/
public interface IFriendService {
    /**
     * 查询好友分组
     * @param query
     * @return
     */
    List<ImFriendGroupDTO> getGroups(UserInfoDTO sessionUser,Map<String, Object> query);

    /**
     * 添加好友分组
     * @param sessionUser 当前用户
     * @param groupName 分组名称
     * @return
     */
    int addGroup(UserInfoDTO sessionUser, String groupName);

    /**
     * 重命名分组
     * @param sessionUser
     * @param groupId
     * @param newName
     * @return
     */
    int renameGroup(UserInfoDTO sessionUser, String groupId, String newName);

    /**
     * 删除分组，默认分组不允许删除；并且，如果分组下存在好友列表，将转移到默认分组下
     * @param sessionUser
     * @param groupId
     * @return
     */
    int removeGroup(UserInfoDTO sessionUser, String groupId);

    /**
     * 添加好友
     * @param sessionUser 当前用户
     * @param friendInfo 好友信息
     * @param remarkName 备注昵称
     * @param groupId 好友分组id
     * @param extraMessage 附加信息
     * @return
     */
    int addFriend(UserInfoDTO sessionUser, UserInfoDTO friendInfo, String remarkName, String groupId,String extraMessage);

    /**
     * 加载好友申请列表
     * @param sessionUser
     * @param query
     * @return
     */
    List<ImFriendAddRequestDTO> listFriendReqs(UserInfoDTO sessionUser, Map<String, Object> query);

    /**
     * 处理好友申请
     * @param sessionUser 当前用户
     * @param senderId 发起人
     * @param suggestionEnum 处理意见
     * @return
     */
    int processFriendAddReq(UserInfoDTO sessionUser, int senderId, FriendAddReqSuggestionEnum suggestionEnum);

    /**
     * 查询好友列表
     * @param sessionUser
     * @param query
     * @return
     */
    List<ImFriendDTO> getFriends(UserInfoDTO sessionUser, Map<String, Object> query);
    /**
     * 删除好友
     * @param deleteParams
     * @return
     */
    int deleteFriend(Map<String, Object> deleteParams);
}
