package com.corecmd.tcloud.tcimserver.mapper;

import com.corecmd.tcloud.tcimserver.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description :
 **/
@Mapper
public interface IFriendMapper {
    /**
     * 添加好友分组
     * @param imFriendGroupDO
     * @return
     */
    int addFriendGroup(ImFriendGroupDO imFriendGroupDO);

    /**
     * 查询好友分组
     * @param query
     * @return
     */
    List<ImFriendGroupDTO> getFriendGroups(Map<String, Object> query);

    /**
     * 获取好友分组中最大排序编号
     * @param userId
     * @return
     */
    int getMaxGroupSortNum(int userId);

    /**
     * 重命名好友分组
     * @param param
     * @return
     */
    int renameGroup(Map<String, Object> param);

    /**
     * 删除指定分组
     * @param groupId
     */
    int deleteGroup(String groupId);

    /**
     * 转移分组下的好友列表
     * @param query  originGroupId：原分组id   targetGroupId:目标分组id  ownerId:所属人id
     * @return
     */
    int switchGroupMembers(Map<String, Object> query);

    /**
     * 添加好友
     * @param newFriend 好友信息
     * @return
     */
    int addFriend(ImFriendDO newFriend);

    /**
     * 添加好友申请记录
     * @param requestDO
     * @return
     */
    int addFriendRequest(ImFriendAddRequestDO requestDO);

    /**
     * 加载好友申请列表
     * @param query
     * @return
     */
    List<ImFriendAddRequestDTO> listFriendReqs(Map<String, Object> query);

    /**
     * 处理好友申请
     * @param params
     * @return
     */
    int processFriendAddReq(Map<String, Object> params);

    /**
     * 删除好友
     * @param deleteParams
     * @return
     */
    int deleteFriend(Map<String, Object> deleteParams);

    /**
     * 查询好友列表
     * @param query
     * @return
     */
    List<ImFriendDTO> getFriends(Map<String, Object> query);

    /**
     * 更新好友关系
     * @param query
     * @return
     */
    int updateFriendRelation(Map<String, Object> query);
}
