package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote : 好友关系状态枚举
 * @date : 2019/12/5
 * @copyright :
 */
public enum FriendRelationStatusEnum {
    NORMAL('0',"正常"),
    UNAGREE('1',"未同意添加"),
    BLACKLIST('2',"黑名单");
    @Setter
    @Getter
    char typeEn;
    @Setter
    @Getter
    String typeCn;
    FriendRelationStatusEnum(char typeEn, String typeCn){
        setTypeEn(typeEn);
        setTypeCn(typeCn);
    }
}
