package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimserver.domain.UserInfoDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description :
 **/
public interface IUserService {
    /**
     * 注册新用户
     * @param newUser
     * @return
     */
    int regist(UserInfoDO newUser) throws Exception;

    /**
     * 查询用户列表信息
     * @param queryParams
     * @return
     */
    List<UserInfoDTO> list(Map<String, Object> queryParams);

    /**
     * 查询用户信息
     * @param userId
     * @return
     */
    UserInfoDTO getUser(int userId);
}
