package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 消息内容类型枚举
 **/
public enum MessageContentTypeEnum {
    PLAINTEXT('1',"文本"),
    IMAGE('2',"图像"),
    AUDIO('3',"音频"),
    VIDEO('4',"视频"),
    FILE('5',"文件");
    @Setter
    @Getter
    char typeEn;
    @Setter
    @Getter
    String typeCn;
    MessageContentTypeEnum(char typeEn,String typeCn){
        this.setTypeEn(typeEn);
        this.setTypeCn(typeCn);
    }
}
