package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Data
public class ImChatGroupMemberDTO extends ImChatGroupMemberDO {
}
