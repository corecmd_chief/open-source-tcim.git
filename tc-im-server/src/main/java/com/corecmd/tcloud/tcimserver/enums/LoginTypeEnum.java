package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
public enum LoginTypeEnum {
    /**
     * 登录方式，密码登录和免密的登录
     */
    PASSWORD("password","密码登录"),
    NOPASSWORD("nopassword","免密登录");

    @Setter
    @Getter
    String code;
    @Setter
    @Getter
    String name;
    LoginTypeEnum(String code, String name){
        setCode(code);
        setName(name);
    }
}
