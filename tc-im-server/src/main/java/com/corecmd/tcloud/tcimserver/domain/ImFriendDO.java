package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 好友
 **/
@Data
public class ImFriendDO {
    //主键id
    private String id;
    //好友昵称
    private String nickName;
    //备注昵称
    private String remarkNickName;
    //好友id
    private int friendId;
    //所属人id
    private int ownerId;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //好友关系状态 0：正常 1：未同意添加 2：黑名单
    private char relationStatus;
    //所属分组
    private String groupId;
    //好友标签
    private String tags;
    //好友备注
    private String description;
}
