package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 消息类型枚举
 **/
public enum MessageTypeEnum {
    ONLINE_NOTIFY('1',"上线通知"),
    OFFLINE_NOTIFY('2',"下线通知"),
    SYS_KICK_OUT_NOTIFY('3',"系统强制踢出"),
    SHAKE_YOU_NOTIFY('4',"窗口抖动"),
    SINGLE_CHAT('5',"单聊"),
    GROUP_CHAT('6',"群聊"),
    SYS_NOTIFY('7',"系统通知"),
    SYS_CLIENT('8',"系统到指定客户端通知");
    @Setter
    @Getter
    char typeEn;
    @Setter
    @Getter
    String typeCn;
    MessageTypeEnum(char typeEn,String typeCn){
        this.setTypeEn(typeEn);
        this.setTypeCn(typeCn);
    }
}
