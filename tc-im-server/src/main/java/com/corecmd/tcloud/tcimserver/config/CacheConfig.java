package com.corecmd.tcloud.tcimserver.config;

import com.corecmd.tcloud.tcimserver.cache.ImCacheManager;
import com.corecmd.tcloud.tcimserver.cache.RedisImCacheManager;
import com.corecmd.tcloud.tcimserver.enums.CacheTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/22
 * @description :
 **/
@Configuration
public class CacheConfig {
    @Value("${spring.cache.type}")
    private String cacheType;
    @Autowired
    private RedisImCacheManager redisCacheManager;
    @Bean
    ImCacheManager imCacheManager(){
        if (CacheTypeEnum.REDIS_CACHE.getTypeEn().equalsIgnoreCase(cacheType)){
            return redisCacheManager;
        }
        return redisCacheManager;
    }
}
