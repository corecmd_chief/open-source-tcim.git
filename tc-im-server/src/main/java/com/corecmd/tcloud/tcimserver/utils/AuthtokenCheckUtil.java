package com.corecmd.tcloud.tcimserver.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.session.mgt.WebSessionKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote : 校验Token是否过期
 * @date : 2020/6/21
 * @copyright :
 */
@Slf4j
public class AuthtokenCheckUtil {

    public static boolean check(String authToken, HttpServletRequest request, HttpServletResponse response){
        boolean isExpried = false;
        if (null != authToken && !authToken.isEmpty()){
            try {
                SessionKey sessionKey = null;
                Session session = null;
                try {
                    sessionKey = new DefaultSessionKey(authToken);
                    session = SecurityUtils.getSecurityManager().getSession(sessionKey);
                } catch (Exception e) {
                    isExpried = true;
                }
                if (null != session){
                    // session没有过期时，校验完毕后，需要刷新一下token
                    session.touch();
                }
            } catch (Exception e) {
                isExpried = true;
            }
        } else {
            isExpried = true;
        }
        return isExpried;
    }

}
