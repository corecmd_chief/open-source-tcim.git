package com.corecmd.tcloud.tcimserver.shiro;

import com.corecmd.tcloud.tcimserver.enums.LoginTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
@Slf4j
public class CustomCredentialsMatcher extends HashedCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        CustomUsernamePasswordToken tk = (CustomUsernamePasswordToken) token;
        if(tk.getLoginTypeEnum().equals(LoginTypeEnum.NOPASSWORD)){
            return true;
        }
        return super.doCredentialsMatch(token,info);
    }
}
