package com.corecmd.tcloud.tcimserver.im.starter;

import com.corecmd.tcloud.tcimcommon.im.common.PacketDecoder;
import com.corecmd.tcloud.tcimcommon.im.common.PacketEncoder;
import com.corecmd.tcloud.tcimserver.im.handler.ChannelDownHandler;
import com.corecmd.tcloud.tcimserver.im.handler.LoginRequestHandler;
import com.corecmd.tcloud.tcimserver.im.handler.MessageRequestHandler;
import com.corecmd.tcloud.tcimserver.im.handler.MessageResponseHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
@Component
public class ImServer {
    @Autowired
    private ObjectFactory<LoginRequestHandler> loginRequestHandlerObjectFactory;
    @Autowired
    private ObjectFactory<MessageRequestHandler> messageRequestHandlerObjectFactory;
    @Autowired
    private ObjectFactory<MessageResponseHandler> messageResponseHandlerObjectFactory;
    @Autowired
    private ObjectFactory<ChannelDownHandler> channelDownHandlerObjectFactory;



    public void start(int port){
        log.info("ImServer启动，正在端口：{}监听",port);
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap
                    .group(bossGroup,workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new PacketDecoder());
                            ch.pipeline().addLast(loginRequestHandlerObjectFactory.getObject());
                            ch.pipeline().addLast(messageRequestHandlerObjectFactory.getObject());
                            ch.pipeline().addLast(messageResponseHandlerObjectFactory.getObject());
                            ch.pipeline().addLast(channelDownHandlerObjectFactory.getObject());
                            ch.pipeline().addLast(new PacketEncoder());
                        }
                    });
            ChannelFuture future = serverBootstrap.bind(port).sync();
            // 等待服务端监听端口关闭
            future.channel().closeFuture().sync();
        } catch (Exception e){
            log.info("服务端异常",e);
        }
        finally {
            // 优雅退出，释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }



















}
