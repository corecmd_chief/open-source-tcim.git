package com.corecmd.tcloud.tcimserver.controller;

import com.corecmd.tcloud.tcimserver.domain.ImFriendGroupDTO;
import com.corecmd.tcloud.tcimserver.domain.ImMessageDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.service.IChatMessageService;
import com.corecmd.tcloud.tcimserver.utils.CustomResponse;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "消息接口")
@RestController
@RequestMapping(value = "/rest/chatMessages")
public class ChatMessageController extends BaseController {
    @Autowired
    private IChatMessageService chatMessageService;

    @ApiOperation("获取会话消息列表")
    @RequestMapping(value = "/{chatSessionId}/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path",name = "chatSessionId",value = "会话id",dataType = "String",required = true),
            @ApiImplicitParam(paramType = "query",name = "pageNum",value = "当前页",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "pageSize",value = "分页大小",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "searchParam",value = "模糊查询参数",dataType = "String",required = false),
            @ApiImplicitParam(paramType = "header",name = "authToken",value = "登录token",dataType = "String",required = true),
    })
    public CustomResponse list(@PathVariable String chatSessionId,@RequestParam int pageNum, @RequestParam int pageSize, @RequestParam(required = false) String searchParam
            , @RequestHeader String authToken, HttpServletRequest request, HttpServletResponse response){
        CustomResponse customResponse = null;
        UserInfoDTO sessionUser = getTokenUser(authToken,request,response);
        if (null != sessionUser){
            Map<String,Object> query = new HashMap<String, Object>(16);
            query.put("searchParam",searchParam);
            query.put("status","0");
            PageHelper.startPage(pageNum,pageSize);
            List<ImMessageDTO> chatMessages = chatMessageService.list(sessionUser, chatSessionId,query);
            PageInfo<ImMessageDTO> pager = new PageInfo<ImMessageDTO>(chatMessages);
            customResponse = CustomResponse.ok();
            customResponse.put("pageNum",pageNum);
            customResponse.put("pageSize",pageSize);
            customResponse.put("chatMessages",pager.getList());
            customResponse.put("totalRecord",pager.getTotal());
            customResponse.put("totalPage",pager.getPages());
        } else {
            customResponse = CustomResponse.error(CustomResponse.CODE_NEED_LOGIN,"登录Token失效，请重新登录获取Token");
        }
        return customResponse;
    }

    @ApiOperation("删除消息")
    @RequestMapping(value = "/{chatSessionId}/{messageId}",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path",name = "chatSessionId",value = "会话id",dataType = "String",required = true),
            @ApiImplicitParam(paramType = "path",name = "messageId",value = "消息id",dataType = "String",required = true),
            @ApiImplicitParam(paramType = "header",name = "authToken",value = "登录token",dataType = "String",required = true),
    })
    public CustomResponse delete(@PathVariable String chatSessionId,@PathVariable String messageId, @RequestHeader String authToken
            , HttpServletRequest request, HttpServletResponse response){
        CustomResponse customResponse = null;
        UserInfoDTO sessionUser = getTokenUser(authToken,request,response);
        if (null != sessionUser){
            int result = chatMessageService.deleteMessage(sessionUser, chatSessionId,messageId);
            customResponse = result>0?CustomResponse.ok("删除成功"):CustomResponse.error("删除失败");
        } else {
            customResponse = CustomResponse.error(CustomResponse.CODE_NEED_LOGIN,"登录Token失效，请重新登录获取Token");
        }
        return customResponse;
    }
}
