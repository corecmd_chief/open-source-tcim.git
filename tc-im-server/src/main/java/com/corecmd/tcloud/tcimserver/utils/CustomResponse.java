package com.corecmd.tcloud.tcimserver.utils;

import java.util.HashMap;
import java.util.Map;
/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 **/
public class CustomResponse extends HashMap<String, Object> {
    //请求成功
    public static final int CODE_REQ_SUCCESS = 200;
    //请求失败
    public static final int CODE_REQ_ERROR = 0;
    // 需要登录
    public static final int CODE_NEED_LOGIN = 401;
    // 没有权限
    public static final int CODE_FORBIDDEN = 403;
    // 找不到目标
    public static final int CODE_NOTFOUND = 404;
    // 服务器内部错误
    public static final int CODE_SERVER_ERROR = 500;
    // 请求参数不合法
    public static final int CODE_BAD_REQ = 400;

    public CustomResponse(){
        put("statusCode", CODE_REQ_SUCCESS);
        put("message", "操作成功");
    }
    public static CustomResponse error() {
        return error(CODE_REQ_ERROR, "操作失败");
    }

    public static CustomResponse error(String msg) {
        return error(CODE_REQ_ERROR, msg);
    }

    public static CustomResponse error(int code, String msg) {
        CustomResponse r = new CustomResponse();
        r.put("statusCode", code);
        r.put("message", msg);
        return r;
    }

    public static CustomResponse ok(String msg) {
        CustomResponse r = new CustomResponse();
        r.put("message", msg);
        return r;
    }

    public static CustomResponse ok(Map<String, Object> map) {
        CustomResponse r = new CustomResponse();
        r.putAll(map);
        return r;
    }
    public static CustomResponse ok(int code, String msg) {
        CustomResponse r = new CustomResponse();
        r.put("statusCode", code);
        r.put("message", msg);
        return r;
    }
    public static CustomResponse ok() {
        return new CustomResponse();
    }

    @Override
    public CustomResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}