package com.corecmd.tcloud.tcimserver.service.impl;

import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDO;
import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDTO;
import com.corecmd.tcloud.tcimserver.domain.ImFriendDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.enums.AvailableStatusEnum;
import com.corecmd.tcloud.tcimserver.enums.ImChatSessionTypeEnum;
import com.corecmd.tcloud.tcimserver.mapper.IChatSessionMapper;
import com.corecmd.tcloud.tcimserver.service.IChatGroupService;
import com.corecmd.tcloud.tcimserver.service.IChatSessionService;
import com.corecmd.tcloud.tcimserver.service.IFriendService;
import com.corecmd.tcloud.tcimserver.utils.UniqueId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Slf4j
@Service
public class ChatSessionServiceImpl implements IChatSessionService {
    @Autowired
    private IChatSessionMapper chatSessionMapper;
    @Autowired
    private IFriendService friendService;
    @Autowired
    private IChatGroupService chatGroupService;

    @Override
    public List<ImChatSessionDTO> getSessions(UserInfoDTO sessionUser, Map<String, Object> queryParam) {
        queryParam.put("ownerId",sessionUser.getId());
        return chatSessionMapper.getSessions(queryParam);
    }

    @Override
    public ImChatSessionDTO createChatSession(UserInfoDTO sessionUser, String receiverId, String sessionType) {
        int sessionUserId = sessionUser.getId();
        ImChatSessionDTO chatSessionDTO = null;
        ImChatSessionTypeEnum sessionTypeEnum = ImChatSessionTypeEnum.match(sessionType.charAt(0));
        if (null != sessionTypeEnum){
            boolean canCreate = false;
            if (ImChatSessionTypeEnum.SINGLE_CHAT.getTypeEn() == sessionTypeEnum.getTypeEn()){
                //查询用户是否是自己的好友
                Map<String,Object> query = new HashMap<String, Object>();
                query.put("friendId",receiverId);
                List<ImFriendDTO> friends = friendService.getFriends(sessionUser,query);
                if (null != friends && friends.size() > 0){
                    canCreate = true;
                } else {
                    log.info("用户：{} 没有添加用户：{} 为好友，发起会话失败!",sessionUserId,receiverId);
                }
            } else if (ImChatSessionTypeEnum.GROUP_CHAT.getTypeEn() == sessionTypeEnum.getTypeEn()){
                //查询用户是否在这个群组
                Map<String,Object> query = new HashMap<String, Object>();
                query.put("memberId",sessionUserId);
                List<UserInfoDTO> members = chatGroupService.getMembers(receiverId,query);
                if (null != members && members.size() > 0){
                    canCreate = true;
                } else {
                    log.info("用户：{} 加入群组：{}，发起会话失败!",sessionUserId,receiverId);
                }
            }
            if (canCreate){
                //保存会话
                ImChatSessionDO chatSessionDO = new ImChatSessionDO();
                chatSessionDO.setId(UniqueId.getUniqueId());
                chatSessionDO.setCreateTime(new Date());
                chatSessionDO.setEnableNotify(AvailableStatusEnum.YES.getCode());
                chatSessionDO.setTopStick(AvailableStatusEnum.NO.getCode());
                chatSessionDO.setStatus(AvailableStatusEnum.YES.getCode());
                chatSessionDO.setOwnerId(sessionUserId);
                chatSessionDO.setReceiverId(receiverId);
                chatSessionDO.setType(sessionTypeEnum.getTypeEn());
                int saveResult = chatSessionMapper.save(chatSessionDO);
                if (saveResult > 0){
                    Map<String,Object> query = new HashMap<String,Object>();
                    query.put("ownerId",sessionUserId);
                    query.put("receiverId",receiverId);
                    List<ImChatSessionDTO> results = chatSessionMapper.getSessions(query);
                    chatSessionDTO = (results != null && results.size() > 0)?results.get(0):null;
                }
            }
        } else {
            log.info("会话类型：{}不正确",sessionType);
        }
        return chatSessionDTO;
    }
}
