package com.corecmd.tcloud.tcimserver.service.impl;

import com.corecmd.tcloud.tcimserver.domain.ImMessageDO;
import com.corecmd.tcloud.tcimserver.domain.ImMessageDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.mapper.IChatMessageMapper;
import com.corecmd.tcloud.tcimserver.service.IChatMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Slf4j
@Service
public class ChatMessageServiceImpl implements IChatMessageService {
    @Autowired
    private IChatMessageMapper chatMessageMapper;


    @Override
    public List<ImMessageDTO> list(UserInfoDTO sessionUser, String chatSessionId, Map<String, Object> query) {
        query.put("ownerId",sessionUser.getId());
        query.put("chatSessionId",chatSessionId);
        return chatMessageMapper.list(query);
    }

    @Override
    public int deleteMessage(UserInfoDTO sessionUser, String chatSessionId, String messageId) {
        Map<String,Object> params = new HashMap<>();
        params.put("ownerId",sessionUser.getId());
        params.put("chatSessionId",chatSessionId);
        params.put("messageId",messageId);
        return chatMessageMapper.deleteMessage(params);
    }

    @Override
    public int save(ImMessageDO messageDO) {
        return chatMessageMapper.save(messageDO);
    }
}
