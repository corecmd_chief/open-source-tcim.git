package com.corecmd.tcloud.tcimserver.mapper;

import com.corecmd.tcloud.tcimserver.domain.ImMessageDO;
import com.corecmd.tcloud.tcimserver.domain.ImMessageDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Mapper
public interface IChatMessageMapper {
    /**
     * 查询消息列表
     * @param query
     * @return
     */
    List<ImMessageDTO> list(Map<String, Object> query);

    /**
     * 删除消息
     * @param params
     * @return
     */
    int deleteMessage(Map<String, Object> params);

    /**
     * 保存消息
     * @param messageDO
     * @return
     */
    int save(ImMessageDO messageDO);
}
