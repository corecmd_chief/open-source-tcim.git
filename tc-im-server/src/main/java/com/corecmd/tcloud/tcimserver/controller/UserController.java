package com.corecmd.tcloud.tcimserver.controller;

import com.corecmd.tcloud.tcimserver.domain.ImFriendGroupDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.enums.AvailableStatusEnum;
import com.corecmd.tcloud.tcimserver.enums.GenderEnum;
import com.corecmd.tcloud.tcimserver.service.IUserService;
import com.corecmd.tcloud.tcimserver.utils.CustomResponse;
import com.corecmd.tcloud.tcimserver.utils.MD5Utils;
import com.corecmd.tcloud.tcimserver.utils.UniqueId;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "用户接口")
@RestController
@RequestMapping(value = "/rest/user")
public class UserController extends BaseController{
    @Autowired
    private IUserService userService;

    @ApiOperation("用户列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "pageNum",value = "当前页",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "pageSize",value = "分页大小",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "searchParam",value = "模糊查询参数",dataType = "String",required = false),
            @ApiImplicitParam(paramType = "header",name = "authToken",value = "登录token",dataType = "String",required = true),
    })
    public CustomResponse list(@RequestParam int pageNum, @RequestParam int pageSize, @RequestParam(required = false) String searchParam
            , @RequestHeader String authToken){
        CustomResponse customResponse = null;
        try {
            Map<String,Object> queryParams = new HashMap<String,Object>(1);
            queryParams.put("searchParam",searchParam);
            PageHelper.startPage(pageNum,pageSize);
            List<UserInfoDTO> users = userService.list(queryParams);
            PageInfo<UserInfoDTO> pager = new PageInfo<UserInfoDTO>(users);
            customResponse = CustomResponse.ok();
            customResponse.put("pageNum",pageNum);
            customResponse.put("pageSize",pageSize);
            customResponse.put("users",pager.getList());
            customResponse.put("totalRecord",pager.getTotal());
            customResponse.put("totalPage",pager.getPages());
        } catch (Exception e) {
            log.info("查询用户列表异常",e);
            customResponse = CustomResponse.error("没有查询到用户列表数据");
        }
        return customResponse;
    }
    @RequestMapping(value = "/validateToken",method = RequestMethod.GET)
    @ApiOperation(value = "校验Token是否有效")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "header",name = "authToken",value = "登录token",dataType = "String",required = true),
    })
    public CustomResponse getTokenUser(@RequestHeader String authToken){
        CustomResponse customResponse = null;
        UserInfoDTO tokenUser = validateToken(authToken);
        if (null != tokenUser){
            customResponse = CustomResponse.ok();
            customResponse.put("tokenInfo",tokenUser);
        } else {
            customResponse = CustomResponse.error("authToken:"+authToken+"无效");
        }
        return customResponse;
    }
}
