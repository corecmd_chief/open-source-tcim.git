package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Data
public class ImChatGroupMemberDO {
    //主键id
    private String id;
    //群组id
    private String chatGroupId;
    //成员id
    private int memberId;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //状态： 0：已加入 1：审核中  2：已拒绝
    private char status;
    //备注信息
    private String description;
    //群角色Id
    private String groupRoleId;
}
