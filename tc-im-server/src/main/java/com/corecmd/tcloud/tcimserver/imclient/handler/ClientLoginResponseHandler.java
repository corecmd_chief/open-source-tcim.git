package com.corecmd.tcloud.tcimserver.imclient.handler;

import com.corecmd.tcloud.tcimcommon.im.domain.LoginResponsePacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
public class ClientLoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket> {
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, LoginResponsePacket loginResponsePacket) throws Exception {
        log.info("客户端收到登陆请求回应："+loginResponsePacket.getMessage());
        channelHandlerContext.flush();
    }
}
