package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimcommon.im.domain.MessageRequestPacket;
import com.corecmd.tcloud.tcimserver.utils.CustomResponse;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/22
 * @description :
 **/
public interface SendMessageToClientsService {
    CustomResponse sendMessage(MessageRequestPacket messagePacket) throws Exception;
}
