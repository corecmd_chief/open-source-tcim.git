package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 好友分组
 **/
@Data
public class ImFriendGroupDO {
    //好友分组id
    private String id;
    //分组名称
    private String name;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //所属人
    private int ownerId;
    //状态 0：可用 1：禁用
    private char status;
    //描述
    private String description;
    //排序号
    private int sortNum;
    //标签
    private String tags;
    //父级分组id
    private String parentId;
    //是否是默认分组 0：是  1：否；默认分组不允许删除
    private char defaultGroup;
}
