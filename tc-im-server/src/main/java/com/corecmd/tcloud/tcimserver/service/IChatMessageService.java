package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimserver.domain.ImMessageDO;
import com.corecmd.tcloud.tcimserver.domain.ImMessageDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
public interface IChatMessageService {
    /**
     * 查询用户的会话消息列表
     * @param sessionUser 当前用户
     * @param query 查询条件
     * @param chatSessionId 会话id
     * @return
     */
    List<ImMessageDTO> list(UserInfoDTO sessionUser, String chatSessionId,Map<String, Object> query);

    /**
     * 删除用户会话中的消息
     * @param sessionUser 当前用户
     * @param chatSessionId 会话id
     * @param messageId 消息id
     * @return
     */
    int deleteMessage(UserInfoDTO sessionUser, String chatSessionId, String messageId);

    /**
     * 新增一条消息
     * @param messageDO
     * @return
     */
    int save(ImMessageDO messageDO);
}
