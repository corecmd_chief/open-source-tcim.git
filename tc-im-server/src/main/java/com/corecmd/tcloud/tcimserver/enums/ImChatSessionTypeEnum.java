package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
public enum ImChatSessionTypeEnum {
    SINGLE_CHAT('1',"单聊"),
    GROUP_CHAT('2',"群聊"),
    SYS_NOTIFY('3',"系统消息");
    @Setter
    @Getter
    char typeEn;
    @Setter
    @Getter
    String typeCn;
    ImChatSessionTypeEnum(char typeEn,String typeCn){
        this.setTypeEn(typeEn);
        this.setTypeCn(typeCn);
    }
    public static ImChatSessionTypeEnum match(char typeEn){
        for (ImChatSessionTypeEnum chatSessionTypeEnum : ImChatSessionTypeEnum.values()) {
            if (chatSessionTypeEnum.getTypeEn() == typeEn){
                return chatSessionTypeEnum;
            }
        }
        return null;
    }
}
