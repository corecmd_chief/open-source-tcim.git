package com.corecmd.tcloud.tcimserver.imclient.handler;

import com.corecmd.tcloud.tcimcommon.im.domain.MessageRequestPacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
public class ClientMessageRequestHandler extends SimpleChannelInboundHandler<MessageRequestPacket> {
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, MessageRequestPacket messageRequestPacket) throws Exception {
        log.info("客户端收到服务端消息请求:"+messageRequestPacket.getMsgContent());
        channelHandlerContext.flush();
    }
}
