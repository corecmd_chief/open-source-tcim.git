package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description : 添加好友请求表
 **/
@Data
public class ImFriendAddRequestDO {
    //主键id
    private String id;
    //好友id
    private int friendId;
    //申请人id
    private int senderId;
    //所属人Id
    private int ownerId;
    //备注信息
    private String remark;
    //申请状态 0：已同意 1：未同意  2：已拒绝
    private char status;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
}
