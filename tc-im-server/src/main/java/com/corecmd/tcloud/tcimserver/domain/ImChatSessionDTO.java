package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Data
public class ImChatSessionDTO extends ImChatSessionDO {
    //会话名称，单聊时，为好友备注名或好友名，群聊时，群备注名或群名，系统消息
    private String name;
}
