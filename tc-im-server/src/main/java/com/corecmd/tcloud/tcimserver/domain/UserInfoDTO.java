package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Data
public class UserInfoDTO extends UserInfoDO implements Serializable {
    //0：小姐姐     1：小哥哥 2：未知
    private String genderCn;
    //0：正常  1：禁用
    private String statusCn;
    private String loginRoleId;
    private String authToken;
}
