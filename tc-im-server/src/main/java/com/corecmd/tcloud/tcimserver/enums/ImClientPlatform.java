package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
public enum ImClientPlatform {
    ANDROID("android","安卓客户端"),
    IPHONE("iphone","苹果客户端"),
    PC("pc","pc客户端"),
    WEB("web","网页客户端");
    @Setter
    @Getter
    String platform_name_en;
    @Setter
    @Getter
    String platform_name_cn;
    ImClientPlatform(String platform_name_en,String platform_name_cn){
        this.setPlatform_name_en(platform_name_en);
        this.setPlatform_name_cn(platform_name_cn);
    }

    public static ImClientPlatform match(String platform_name_en){
        for (ImClientPlatform platform: ImClientPlatform.values()) {
            if (platform.getPlatform_name_en().equals(platform_name_en)){
                return platform;
            }
        }
        return null;
    }
}
