package com.corecmd.tcloud.tcimserver.controller;

import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.service.IChatSessionService;
import com.corecmd.tcloud.tcimserver.utils.CustomResponse;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "会话接口")
@RestController
@RequestMapping(value = "/rest/chatSession")
public class ChatSessionController extends BaseController {
    @Autowired
    private IChatSessionService chatSessionService;


    @ApiOperation(value = "发起聊天会话")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "receiverId", value = "会话消息接收方id，用户或群组或系统", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "sessionType", value = "会话类型 1：单聊 2：群聊 3：系统消息", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "header", name = "authToken", value = "登录Token", dataType = "String", required = true)
    })
    public CustomResponse addChatSession(@RequestParam String receiverId, @RequestParam String sessionType
            , @RequestHeader String authToken, HttpServletRequest request, HttpServletResponse response){
        CustomResponse customResponse = null;
        UserInfoDTO sessionUser = getTokenUser(authToken,request,response);
        Map<String,Object> queryParam = new HashMap<>(1);
        queryParam.put("receiverId",receiverId);
        List<ImChatSessionDTO> chatSessions = chatSessionService.getSessions(sessionUser,queryParam);
        if (null != chatSessions && chatSessions.size() > 0){
            customResponse = CustomResponse.ok("会话已存在，跳过创建");
            customResponse.put("chatSession",chatSessions.get(0));
        } else {
            ImChatSessionDTO chatSession = chatSessionService.createChatSession(sessionUser,receiverId,sessionType);
            if (null != chatSession){
                customResponse = CustomResponse.ok("会话创建成功");
                customResponse.put("chatSession",chatSession);
            } else {
                customResponse = CustomResponse.error("创建会话失败");
            }
        }
        return customResponse;
    }

    @ApiOperation(value = "会话列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "pageNum",value = "当前页",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "pageSize",value = "分页大小",dataType = "int",required = true),
            @ApiImplicitParam(paramType = "query",name = "searchParam",value = "模糊查询参数",dataType = "String",required = false),
            @ApiImplicitParam(paramType = "header",name = "authToken",value = "登录token",dataType = "String",required = true),
    })
    public CustomResponse listChatSessions(@RequestParam int pageNum, @RequestParam int pageSize, @RequestParam(required = false) String searchParam
            , @RequestHeader String authToken, HttpServletRequest request, HttpServletResponse response){
        CustomResponse customResponse = null;
        UserInfoDTO sessionUser = getTokenUser(authToken,request,response);
        if (null != sessionUser){
            Map<String,Object> query = new HashMap<String, Object>(16);
            query.put("searchParam",searchParam);
            PageHelper.startPage(pageNum,pageSize);
            List<ImChatSessionDTO> chatSessions = chatSessionService.getSessions(sessionUser,query);
            PageInfo<ImChatSessionDTO> pager = new PageInfo<ImChatSessionDTO>(chatSessions);
            customResponse = CustomResponse.ok();
            customResponse.put("pageNum",pageNum);
            customResponse.put("pageSize",pageSize);
            customResponse.put("chatSessions",pager.getList());
            customResponse.put("totalRecord",pager.getTotal());
            customResponse.put("totalPage",pager.getPages());
        } else {
            customResponse = CustomResponse.error(CustomResponse.CODE_NEED_LOGIN,"登录Token失效，请重新登录获取Token");
        }
        return customResponse;
    }





}
