package com.corecmd.tcloud.tcimserver.shiro;

import com.corecmd.tcloud.tcimserver.enums.LoginTypeEnum;
import lombok.Data;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
@Data
public class CustomUsernamePasswordToken extends UsernamePasswordToken {
    /**
     * 登录方式，密码登录或免密登录
     */
    private LoginTypeEnum loginTypeEnum;
    //登录角色Id
    private String loginRoleId;

    public CustomUsernamePasswordToken(LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super();
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, char[] password, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, String password, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, char[] password, String host, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, host);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, String password, String host, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, host);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, char[] password, boolean rememberMe, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, rememberMe);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, String password, boolean rememberMe, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, rememberMe);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, char[] password, boolean rememberMe, String host, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, rememberMe, host);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }

    public CustomUsernamePasswordToken(String username, String password, boolean rememberMe, String host, LoginTypeEnum loginTypeEnum, String loginRoleId) {
        super(username, password, rememberMe, host);
        this.loginTypeEnum = loginTypeEnum;
        this.loginRoleId = loginRoleId;
    }
}
