package com.corecmd.tcloud.tcimserver.imclient.starter;

import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcimserver.utils.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
public class StartupClient {
    public static void main(String[] args) {
        String userId = "9";
        String userName = "客户端9";
        String password = "Tian2334227..";
        String imHost = "";
        int imPort = 0;
        //登录
        String loginUrl = "http://localhost:7400/v1/im/login/imLogin";
        Map<String,String> params = new HashMap<>();
        params.put("userId",userId);
        params.put("password",password);
        try {
            String loginResult = HttpClientUtils.httpFormPost(loginUrl,params,null,null);
            log.info("登录结果："+loginResult);
            JSONObject jsonResult = JSONObject.parseObject(loginResult);
            if ("200".equals(jsonResult.getString("statusCode"))){
                imHost = jsonResult.getString("imHost");
                imPort = jsonResult.getInteger("imPort");
                new ImClient(userId,userName).start(imHost,imPort);
            }
        } catch (Exception e) {
            log.info("登录im异常",e);
        }
    }
}
