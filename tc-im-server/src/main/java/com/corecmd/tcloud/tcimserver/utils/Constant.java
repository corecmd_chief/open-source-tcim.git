package com.corecmd.tcloud.tcimserver.utils;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
public class Constant {
    //停止计划任务
    public static String STATUS_RUNNING_STOP = "stop";
    //开启计划任务
    public static String STATUS_RUNNING_START = "start";
    //通知公告阅读状态-未读
    public static String OA_NOTIFY_READ_NO = "0";
    //通知公告阅读状态-已读
    public static int OA_NOTIFY_READ_YES = 1;
    //部门根节点id
    public static String DEPT_ROOT_ID = "0";
    //缓存方式
    public static String CACHE_TYPE_REDIS ="redis";
    /**
     * 用户默认头像
     */
    public static String USER_DEFAULT_PICURL = "http://www.tianshaojiao.com/ajoefileserver/M00/00/1C/Co0Xt13syc2AezWHAAGDJ70Xwzw104.png";

    public static String LOG_ERROR = "error";
    public static final String FILE_TYPE_DICT = "sys_file_type_dict";
    public static final String FILE_SERVER_PREFIX = "http://www.tianshaojiao.com/";
    public static final String FILE_SERVER_PREFIX2 = "http://123.57.62.114/";
    /**
     * 普通用户-工单分类
     */
    public static final String ORDINARYUSER_ORDERSORT = "OrdinaryUser_OrderSort";
    /**
     * 管理员-工单分类
     */
    public static final String ADMINUSER_ORDERSORT = "AdminUser_OrderSort";
    public static final String OrdinaryUserRoleId = "90000";
    //我的申请-工单状态
    public static final String ORDERSTATUSTYPE_MYAPPLY = "myApply_orderStatus";
    //我的审批-工单状态
    public static final String ORDERSTATUSTYPE_MYAPPROVE = "myApprove_orderStatus";
    //全部工单-工单状态
    public static final String ORDERSTATUSTYPE_ALLORDER = "allOrder_orderStatus";
    // 普通用户-工单分类  我的申请
    public static final String ORDERSORT_MYAPPLY = "myApply";
    //管理员-工单分类   我的审批
    public static final String ORDERSORT_MYAPPROVE = "myApprove";
    //管理员-工单分类   全部工单
    public static final String ORDERSORT_ALLORDER = "allOrder";

    public static final String SERVER_NOT_AVAILABLE = "服务不可达";

    /** 参数校验失败**/
    public static final String VALIDATION_FAIL = "参数校验失败";

    public static final String DIARY_MOOD_DICTTYPE = "moodType";
    public static final String DIARY_WEATHER_DICTTYPE = "weatherType";

}
