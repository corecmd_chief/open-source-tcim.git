package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
public interface IChatGroupService {
    /**
     * 查询聊天群组成员列表
     * @param groupId
     * @param query
     * @return
     */
    List<UserInfoDTO> getMembers(String groupId, Map<String, Object> query);
}
