package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description : 加群请求
 **/
@Data
public class ImChatGroupAddRequestDO {
    //主键id
    private String id;
    //群组id
    private String groupId;
    //申请人id
    private int senderId;
    //备注信息
    private String remark;
    //申请状态 0：已同意 1：未同意  2：已拒绝
    private char status;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
}
