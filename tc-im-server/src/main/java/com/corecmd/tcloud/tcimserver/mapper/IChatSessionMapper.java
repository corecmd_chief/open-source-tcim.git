package com.corecmd.tcloud.tcimserver.mapper;

import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDO;
import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Mapper
public interface IChatSessionMapper {
    /**
     * 查询会话列表
     * @param queryParam 查询参数
     * @return
     */
    List<ImChatSessionDTO> getSessions(Map<String, Object> queryParam);

    /**
     * 保存会话
     * @param chatSessionDO
     * @return
     */
    int save(ImChatSessionDO chatSessionDO);
}
