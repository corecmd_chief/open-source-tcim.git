package com.corecmd.tcloud.tcimserver.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/22
 * @description :
 **/
@Slf4j
@Component
public class RedisImCacheManager implements ImCacheManager {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public boolean setKeyValue(String key, String value) {
        redisTemplate.opsForValue().set(key,value);
        return true;
    }

    @Override
    public boolean deleteKey(String key){
        return redisTemplate.delete(key);
    }

    @Override
    public String getValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 获取指定前缀的值
     * @param prefix key前缀
     * @return
     */
    public List<String> getPrefixKeyValue(String prefix) {
        List<String> values = null;
        // 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);
        if (null != keys){
            // 批量获取数据
           values = redisTemplate.opsForValue().multiGet(keys);
        }
        return values;
    }
}
