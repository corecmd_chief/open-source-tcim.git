package com.corecmd.tcloud.tcimserver.service;

import com.corecmd.tcloud.tcimserver.domain.ImChatSessionDTO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
public interface IChatSessionService {
    /**
     * 查询用户的会话列表
     * @param sessionUser 当前用户
     * @param queryParam 查询参数
     * @return
     */
    List<ImChatSessionDTO> getSessions(UserInfoDTO sessionUser, Map<String, Object> queryParam);

    /**
     * 创建新会话
     * @param sessionUser 当前用户
     * @param receiverId 接收方id  可以是用户id   群组id
     * @param sessionType 会话类型  单聊 群聊  系统消息
     * @return
     */
    ImChatSessionDTO createChatSession(UserInfoDTO sessionUser, String receiverId, String sessionType);
}
