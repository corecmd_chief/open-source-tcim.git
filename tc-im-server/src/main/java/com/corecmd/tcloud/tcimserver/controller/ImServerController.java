package com.corecmd.tcloud.tcimserver.controller;

import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcimcommon.im.domain.MessageRequestPacket;
import com.corecmd.tcloud.tcimserver.service.SendMessageToClientsService;
import com.corecmd.tcloud.tcimserver.utils.CustomResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Slf4j
@Api(tags = "消息推送")
@CrossOrigin
@RestController
@RequestMapping(value = "/rest/imServer")
public class ImServerController extends BaseController {
    @Autowired
    private SendMessageToClientsService sendMessageToClientsService;

    @ApiOperation("推送消息到本机客户端")
    @RequestMapping(value = "/pushMessage",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "senderId", value = "发送人id", dataType = "int", required = true),
            @ApiImplicitParam(paramType = "query", name = "receiverId", value = "接收人id", dataType = "int", required = true),
            @ApiImplicitParam(paramType = "query", name = "msgContent", value = "消息内容", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "msgType", value = "消息类型", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "msgContentType", value = "消息内容类型", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "sendTimestamp", value = "时间戳", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "header", name = "authToken", value = "登录token", dataType = "String", required = true)
    })
    private String sendImMessage(@RequestParam int senderId, @RequestParam int receiverId, @RequestParam String msgContent
            , @RequestParam String msgType, @RequestParam String msgContentType, @RequestParam String sendTimestamp
            , @RequestHeader String authToken, HttpServletRequest request, HttpServletResponse response){
        log.info("推送消息到连接本服务器的客户端:"+receiverId+",消息内容："+msgContent);
        CustomResponse customResponse = null;
        try {
            MessageRequestPacket messagePacket = new MessageRequestPacket();
            messagePacket.setReceiverId(String.valueOf(receiverId));
            messagePacket.setSenderId(String.valueOf(senderId));
            messagePacket.setMsgContent(msgContent);
            messagePacket.setMsgType(msgType);
            messagePacket.setMsgContentType(msgContentType);
            messagePacket.setSendTimestamp(sendTimestamp);
            customResponse = sendMessageToClientsService.sendMessage(messagePacket);
        } catch (Exception e) {
            log.info("消息推送异常",e);
            customResponse = CustomResponse.error(CustomResponse.CODE_SERVER_ERROR,"消息推送失败");
        }
        return JSONObject.toJSONString(customResponse);
    }
}
