package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote : 性别枚举
 * @date : 2019/12/5
 * @copyright :
 */
public enum GenderEnum {
    MALE("0","小姐姐"),
    FEMALE("1","小哥哥"),
    UNKNOWN("2","未知");
    @Setter
    @Getter
    String gender;
    @Setter
    @Getter
    String genderCn;
    GenderEnum(String gender, String genderCn){
        setGender(gender);
        setGenderCn(genderCn);
    }
}
