package com.corecmd.tcloud.tcimserver.utils;


import com.corecmd.tcloud.tcimserver.enums.DayOfWeekEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/12/1
 * @copyright :
 */
public class DateUtil {

    public static DayOfWeekEnum getDayOfWeek(Date currentDate){
        DayOfWeekEnum dayOfWeekEnum = null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        // 指示一个星期中的某天。
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w <= 0){
            dayOfWeekEnum = DayOfWeekEnum.SUNDARY;
        } else {
            dayOfWeekEnum = DayOfWeekEnum.match(String.valueOf(w).charAt(0));
        }
        return dayOfWeekEnum;
    }
    public static void main(String[] args) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse("2020-03-11");
            System.out.println(getDayOfWeek(date).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
