package com.corecmd.tcloud.tcimserver.service.impl;

import com.corecmd.tcloud.tcimserver.domain.ImFriendGroupDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import com.corecmd.tcloud.tcimserver.enums.AvailableStatusEnum;
import com.corecmd.tcloud.tcimserver.mapper.IFriendMapper;
import com.corecmd.tcloud.tcimserver.mapper.IUserMapper;
import com.corecmd.tcloud.tcimserver.service.IUserService;
import com.corecmd.tcloud.tcimserver.utils.UniqueId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/26
 * @description :
 **/
@Slf4j
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private IUserMapper userMapper;
    @Autowired
    private IFriendMapper imFriendMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int regist(UserInfoDO newUser) throws Exception{
        int result = userMapper.save(newUser);
        //给用户创建默认好友分组
        if (result > 0){
            ImFriendGroupDO defaultFriendGroup = new ImFriendGroupDO();
            defaultFriendGroup.setId(UniqueId.getUniqueId());
            defaultFriendGroup.setName("我的好友");
            defaultFriendGroup.setOwnerId(newUser.getId());
            defaultFriendGroup.setCreateTime(new Date());
            defaultFriendGroup.setSortNum(0);
            defaultFriendGroup.setStatus(AvailableStatusEnum.YES.getCode());
            defaultFriendGroup.setParentId("0");
            defaultFriendGroup.setDefaultGroup(AvailableStatusEnum.YES.getCode());
            int addResult = imFriendMapper.addFriendGroup(defaultFriendGroup);
            result += addResult;
        }
        return result;
    }

    @Override
    public List<UserInfoDTO> list(Map<String, Object> queryParams) {
        return userMapper.list(queryParams);
    }

    @Override
    public UserInfoDTO getUser(int userId) {
        UserInfoDTO userInfoDTO = null;
        try {
            userInfoDTO = userMapper.getUserById(userId);
        } catch (Exception e) {
            log.info("查询用户信息异常",e);
        }
        return userInfoDTO;
    }
}
