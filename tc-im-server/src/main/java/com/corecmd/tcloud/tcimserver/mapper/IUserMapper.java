package com.corecmd.tcloud.tcimserver.mapper;

import com.corecmd.tcloud.tcimserver.domain.UserInfoDO;
import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Mapper
public interface IUserMapper {
    /**
     * 根据用户id查询用户
     * @param userId
     * @return
     */
    UserInfoDTO getUserById(int userId);

    /**
     * 保存用户信息
     * @param userInfoDO
     * @return
     */
    int save(UserInfoDO userInfoDO);

    /**
     * 查询用户列表
     * @param queryParams
     * @return
     */
    List<UserInfoDTO> list(Map<String, Object> queryParams);
}
