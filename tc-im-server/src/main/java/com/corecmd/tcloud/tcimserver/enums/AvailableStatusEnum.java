package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
public enum AvailableStatusEnum {
    /**
     * 是否可用状态标记
     */
    YES('0',"可用"),
    NO('1',"不可用");
    @Setter
    @Getter
    private Character code;
    @Setter
    @Getter
    private String name;
    AvailableStatusEnum(Character code, String name){
        setCode(code);
        setName(name);
    }
}
