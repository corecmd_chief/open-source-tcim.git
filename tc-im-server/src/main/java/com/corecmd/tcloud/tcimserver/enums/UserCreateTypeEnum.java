package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2019/12/5
 * @copyright :
 */
public enum UserCreateTypeEnum {
    PORTAL('0',"网站用户"),
    WECHAT('1',"微信用户"),
    OTHERS('2',"其他平台用户");
    @Setter
    @Getter
    char typeEn;
    @Setter
    @Getter
    String typeCn;
    UserCreateTypeEnum(char typeEn, String typeCn){
        setTypeEn(typeEn);
        setTypeCn(typeCn);
    }
}
