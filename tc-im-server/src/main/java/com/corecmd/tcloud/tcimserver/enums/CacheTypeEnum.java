package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/22
 * @description :
 **/
public enum CacheTypeEnum {
    EHCACHE_CACHE("ehcache","ehcache"),
    REDIS_CACHE("redis","redis");
    @Setter
    @Getter
    String typeEn;
    @Setter
    @Getter
    String typeCn;
    CacheTypeEnum(String typeEn,String typeCn){
        setTypeEn(typeEn);
        setTypeCn(typeCn);
    }
}
