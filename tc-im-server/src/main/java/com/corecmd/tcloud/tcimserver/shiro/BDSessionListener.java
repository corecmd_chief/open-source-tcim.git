package com.corecmd.tcloud.tcimserver.shiro;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/6/21
 * @copyright :
 */
@Slf4j
@Component
public class BDSessionListener implements SessionListener {


    private final AtomicInteger sessionCount = new AtomicInteger(0);

    @Override
    public void onStart(Session session) {
        sessionCount.incrementAndGet();
    }

    @Override
    public void onStop(Session session) {
        sessionCount.decrementAndGet();
    }

    @Override
    public void onExpiration(Session session) {
        sessionCount.decrementAndGet();
        String token = String.valueOf(session.getId());
        log.info("检测到会话：{}已经过期：",token);
        try {
        } catch (Exception e) {
            log.info("删除登录信息表中的会话:{},异常",token,e);
        }
    }

    public int getSessionCount() {
        return sessionCount.get();
    }
}
