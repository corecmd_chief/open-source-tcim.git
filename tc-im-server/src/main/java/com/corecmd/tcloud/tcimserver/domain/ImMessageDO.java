package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 消息，单聊消息、群聊消息、系统通知等，消息属于某个会话
 **/
@Data
public class ImMessageDO {
    //消息id
    private String id;
    //所属会话
    private String chatSessionId;
    //发送人id
    private int senderId;
    //接收人id
    private int receiverId;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //消息状态 0：启用  1：禁用
    private char status;
    //阅读状态 0：已读  1：未读
    private char readFlag;
    //消息类型 1：上线通知 2：下线通知  3：系统强制踢出 4：窗口抖动 5：单聊 6: 群聊 7:系统通知
    private char messageType;
    //消息内容类型 1：文本 2：图像  3：音频 4：视频 5：文件等
    private char contentType;
    //消息内容，非文本时，保存的是文件链接
    private String msgContent;
}
