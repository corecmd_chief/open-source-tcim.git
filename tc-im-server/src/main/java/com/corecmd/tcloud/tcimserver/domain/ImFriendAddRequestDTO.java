package com.corecmd.tcloud.tcimserver.domain;

import lombok.Data;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/27
 * @apiNote :
 */
@Data
public class ImFriendAddRequestDTO extends ImFriendAddRequestDO {
}
