package com.corecmd.tcloud.tcimserver.cache;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/22
 * @description :
 **/
public interface ImCacheManager {
    boolean setKeyValue(String key,String value);
    boolean deleteKey(String key);
    String getValue(String key);
}
