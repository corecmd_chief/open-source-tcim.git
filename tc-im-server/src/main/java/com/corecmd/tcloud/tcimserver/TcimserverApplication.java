package com.corecmd.tcloud.tcimserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@EnableDiscoveryClient
@SpringBootApplication
@EnableHystrix
@EnableCaching
public class TcimserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(TcimserverApplication.class, args);
    }

}
