package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2019/8/2
 * @copyright :
 */
public enum FileTypeEnum {
    IMAGE('1',"图片"),
    VIDEO('2',"视频"),
    PDF('3',"pdf文件"),
    EXCEL('4',"excel文件"),
    DOC('5',"word文件"),
    PPT('6',"ppt演示文档"),
    OTHER('7',"其他类型文档"),
    EXE('8',".exe可执行文件");
    @Setter
    @Getter
    char fileTypeEn;
    @Setter
    @Getter
    String fileTypeCn;
    FileTypeEnum(char fileTypeEn, String fileTypeCn){
        setFileTypeEn(fileTypeEn);
        setFileTypeCn(fileTypeCn);
    }
    public static FileTypeEnum match(String fileExtendName){
        FileTypeEnum fileType = FileTypeEnum.OTHER;
        if (".png".equalsIgnoreCase(fileExtendName) || ".jpg".equalsIgnoreCase(fileExtendName)
                || ".jpeg".equalsIgnoreCase(fileExtendName) || ".bmp".equalsIgnoreCase(fileExtendName)
                || ".gif".equalsIgnoreCase(fileExtendName)){
            fileType = FileTypeEnum.IMAGE;
        }else if (".mp4".equalsIgnoreCase(fileExtendName) || ".vi".equalsIgnoreCase(fileExtendName)
                || ".rmvb".equalsIgnoreCase(fileExtendName) || ".mpg".equalsIgnoreCase(fileExtendName)
                || ".mpeg".equalsIgnoreCase(fileExtendName)){
            fileType = FileTypeEnum.VIDEO;
        } else if (".pdf".equalsIgnoreCase(fileExtendName)){
            fileType = FileTypeEnum.PDF;
        } else if(".xls".equalsIgnoreCase(fileExtendName) || ".xlsx".equalsIgnoreCase(fileExtendName)
                || ".csv".equalsIgnoreCase(fileExtendName)){
            fileType = FileTypeEnum.EXCEL;
        } else if (".doc".equalsIgnoreCase(fileExtendName) || ".docx".equalsIgnoreCase(fileExtendName)) {
            fileType = FileTypeEnum.DOC;
        } else if(".ppt".equalsIgnoreCase(fileExtendName) || ".pptx".equalsIgnoreCase(fileExtendName)){
            fileType = FileTypeEnum.PPT;
        }
        return fileType;
    }
}
