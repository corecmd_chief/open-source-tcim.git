package com.corecmd.tcloud.tcimserver.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2019/8/7
 * @copyright :
 */
public class UniqueId {
    public static String getUniqueId(){
        return UUID.randomUUID().toString();
    }

    public static void main(String[] args) {
         SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        System.out.println(sdf.format(new Date()));
        Pattern pattern = Pattern.compile("[\\s\\\\/:\\*\\?\\\"<>\\|]");
        Matcher matcher = pattern.matcher("fasdfafadf. fasdf /// fasfqarwg  dfargaqrg");
        System.out.println(matcher.replaceAll(""));
    }
}
