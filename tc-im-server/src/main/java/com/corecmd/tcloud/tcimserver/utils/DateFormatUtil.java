package com.corecmd.tcloud.tcimserver.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2020/8/27
 * @copyright :
 */
public class DateFormatUtil {
    private static final String dateFormatStr = "yyyymmddHHmmssSSS";

    private static SimpleDateFormat sdf = new SimpleDateFormat(dateFormatStr);

    public static String getCurrentDateStr(){
        return sdf.format(new Date());
    }

    public static void main(String[] args) {
        System.out.println(DateFormatUtil.getCurrentDateStr());
    }
}
