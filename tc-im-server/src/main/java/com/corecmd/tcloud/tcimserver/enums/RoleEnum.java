package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote : 角色枚举
 * @date : 2020/12/6
 * @copyright :
 */
public enum RoleEnum {
    COMMON("90000","普通用户"),
    SUPER_ADMIN("10000","超级管理员");
    @Setter
    @Getter
    String roleId;
    @Setter
    @Getter
    String roleName;
    RoleEnum(String roleId,String roleName){
        setRoleId(roleId);
        setRoleName(roleName);
    }
    public static RoleEnum match(String roleId){
        RoleEnum roleEnum = null;
        for (RoleEnum role : RoleEnum.values()) {
            if (role.getRoleId().equalsIgnoreCase(roleId)){
                roleEnum = role;
                break;
            }
        }
        return roleEnum;
    }
}
