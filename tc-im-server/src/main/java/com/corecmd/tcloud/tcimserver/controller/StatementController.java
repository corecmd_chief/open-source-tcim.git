package com.corecmd.tcloud.tcimserver.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "朋友圈接口")
@RestController
@RequestMapping(value = "/rest/statement")
public class StatementController extends BaseController {
    @RequestMapping(value = "/sayHello",method = RequestMethod.GET)
    private String sayHello(){
        return "Hello";
    }
}
