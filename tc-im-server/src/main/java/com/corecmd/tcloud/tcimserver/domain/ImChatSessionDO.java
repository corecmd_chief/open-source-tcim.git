package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 会话，可以是单聊会话，群聊会话，系统消息会话等
 **/
@Data
public class ImChatSessionDO {
    //会话id
    private String id;
    //所属人
    private int ownerId;
    //会话消息接收人方，好友 或者群等
    private String receiverId;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //会话类型，单聊、群聊、系统消息等
    private char type;
    //会话状态 0：可用  1：禁用
    private char status;
    //是否置顶 0：置顶  1：不置顶
    private char topStick;
    //是否启用消息通知，即消息免打扰 0：开启  1：关闭
    private char enableNotify;
}
