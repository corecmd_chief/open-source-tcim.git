package com.corecmd.tcloud.tcimserver.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description : 群聊
 **/
@Data
public class ImChatGroupDO {
    //群id
    private String id;
    //群名称
    private String groupName;
    //创建时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //更新时间
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //群头像
    private String avatarUrl;
    //群备注
    private String description;
    //群标签
    private String tags;
    //群状态 0：可用  1：不可用
    private char status;
    //是否全员禁言 0:是  1：否
    private char chatForbidden;
    //创建人id
    private String creatorId;
    //群主id
    private int ownerId;
}
