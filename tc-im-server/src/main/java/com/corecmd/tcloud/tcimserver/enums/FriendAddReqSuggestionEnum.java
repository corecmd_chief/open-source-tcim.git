package com.corecmd.tcloud.tcimserver.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/27
 * @apiNote :
 */
public enum FriendAddReqSuggestionEnum {
    AGREE("0","同意"),
    REFUSE("1","拒绝");
    @Setter
    @Getter
    String typeEn;
    @Setter
    @Getter
    String typeCn;
    FriendAddReqSuggestionEnum(String typeEn,String typeCn){
        this.setTypeEn(typeEn);
        this.setTypeCn(typeCn);
    }

    public static FriendAddReqSuggestionEnum match(String typeEn){
        for (FriendAddReqSuggestionEnum suggestionEnum: FriendAddReqSuggestionEnum.values()) {
            if (suggestionEnum.getTypeEn().equals(typeEn)){
                return suggestionEnum;
            }
        }
        return null;
    }
}
