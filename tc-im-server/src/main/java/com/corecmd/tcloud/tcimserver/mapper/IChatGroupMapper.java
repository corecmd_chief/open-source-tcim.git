package com.corecmd.tcloud.tcimserver.mapper;

import com.corecmd.tcloud.tcimserver.domain.UserInfoDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/27
 * @copyright :
 */
@Mapper
public interface IChatGroupMapper {
    /**
     * 查询群组成员
     * @param query
     * @return
     */
    List<UserInfoDTO> getMembers(Map<String, Object> query);
}
