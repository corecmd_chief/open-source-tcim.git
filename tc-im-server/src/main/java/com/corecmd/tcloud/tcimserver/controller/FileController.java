package com.corecmd.tcloud.tcimserver.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "文件接口")
@RestController
@RequestMapping(value = "/rest/file")
public class FileController extends BaseController {
    @RequestMapping(value = "/sayHello",method = RequestMethod.GET)
    private String sayHello(){
        return "Hello";
    }
}
