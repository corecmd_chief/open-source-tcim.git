package com.corecmd.tcloud.tcimserver.controller;

import com.corecmd.tcloud.tcimserver.service.IChatGroupService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/25
 * @description :
 **/
@Slf4j
@Api(tags = "群组接口")
@RestController
@RequestMapping(value = "/rest/chatGroup")
public class ChatGroupController extends BaseController{
    @Autowired
    private IChatGroupService chatGroupService;

    @RequestMapping(value = "/sayHello",method = RequestMethod.GET)
    private String sayHello(){
        return "Hello";
    }
}
