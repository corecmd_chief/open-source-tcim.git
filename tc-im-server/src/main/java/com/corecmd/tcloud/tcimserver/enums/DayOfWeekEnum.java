package com.corecmd.tcloud.tcimserver.enums;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote : 星期枚举
 * @date : 2020/12/1
 * @copyright :
 */
public enum DayOfWeekEnum {
    SUNDARY('7',"星期日"),
    MONDAY('1',"星期一"),
    TUESDAY('2',"星期二"),
    WEDNESDAY('3',"星期三"),
    THURSDAY('4',"星期四"),
    FRIDAY('5',"星期五"),
    SATURDAY('6',"星期六");
    char dayEn;
    String dayCn;

    DayOfWeekEnum(char dayEn,String dayCn){
        setDayEn(dayEn);
        setDayCn(dayCn);
    }
    public static DayOfWeekEnum match(char dayEn){
        DayOfWeekEnum dayWeek = null;
        for (DayOfWeekEnum day:DayOfWeekEnum.values()) {
            if (day.getDayEn() == dayEn){
                dayWeek = day;
                break;
            }
        }
        return dayWeek;
    }

    @Override
    public String toString() {
        return "DayOfWeekEnum{" +
                "dayEn=" + dayEn +
                ", dayCn='" + dayCn + '\'' +
                '}';
    }

    public char getDayEn() {
        return dayEn;
    }

    public void setDayEn(char dayEn) {
        this.dayEn = dayEn;
    }

    public String getDayCn() {
        return dayCn;
    }

    public void setDayCn(String dayCn) {
        this.dayCn = dayCn;
    }
}
