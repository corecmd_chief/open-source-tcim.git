package com.corecmd.tcloud.tcimcommon.im.common;

import com.alibaba.fastjson.JSON;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class JSONSerializer implements Serializer {
    @Override
    public byte getSerializerAlgorithm() {
        return SerializerAlgorithm.JSON;
    }

    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}
