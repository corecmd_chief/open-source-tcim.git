package com.corecmd.tcloud.tcimcommon.im.domain;


import lombok.Data;

import static com.corecmd.tcloud.tcimcommon.im.common.Command.MESSAGE_REQUEST;


/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Data
public class MessageRequestPacket extends Packet {
    //发送人id
    private String senderId;
    //接收人id
    private String receiverId;
    //消息内容
    private String msgContent;
    //消息类型
    private String msgType;
    //消息内容类型
    private String msgContentType;
    //消息时间戳
    private String sendTimestamp;
    @Override
    public Byte getCommand() {
        return MESSAGE_REQUEST;
    }
}
