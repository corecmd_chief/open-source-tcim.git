package com.corecmd.tcloud.tcimcommon.im.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public interface Command {
    Byte LOGIN_REQUEST = 1;
    Byte MESSAGE_REQUEST = 2;
    Byte MESSAGE_RESPONSE = 3;
    Byte LOGIN_RESPONSE = 4;
}
