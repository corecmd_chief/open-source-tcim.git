package com.corecmd.tcloud.tcimcommon.im.domain;


import lombok.Data;

import static com.corecmd.tcloud.tcimcommon.im.common.Command.LOGIN_RESPONSE;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Data
public class LoginResponsePacket extends Packet {
    private int statusCode;
    private String message;
    @Override
    public Byte getCommand() {
        return LOGIN_RESPONSE;
    }
}
