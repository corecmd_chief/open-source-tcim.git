package com.corecmd.tcloud.tcimcommon.im.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public interface MessageContentType {
    //文本
    String PLAINTEXT = "1";
    //图像
    String IMAGE = "2";
    //音频
    String AUDIO = "3";
    //视频
    String VIDEO = "4";
    //文件
    String FILE = "5";
}
