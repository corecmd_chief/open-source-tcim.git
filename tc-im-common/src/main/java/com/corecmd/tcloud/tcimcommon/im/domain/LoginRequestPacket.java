package com.corecmd.tcloud.tcimcommon.im.domain;

import lombok.Data;

import static com.corecmd.tcloud.tcimcommon.im.common.Command.LOGIN_REQUEST;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Data
public class LoginRequestPacket extends Packet {
    private String userId;
    private String username;
    private String password;
    //客户端类型, android|iphone|pc|web
    private String platform;

    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}
