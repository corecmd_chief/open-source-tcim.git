package com.corecmd.tcloud.tcimcommon.im.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public interface MessageType {
    //上线通知
    String ONLINE_NOTIFY = "1";
    //下线通知
    String OFFLINE_NOTIFY = "2";
    //系统强制踢出
    String SYS_KICK_OUT_NOTIFY = "3";
    //窗口抖动
    String SHAKE_YOU_NOTIFY = "4";
    //单聊
    String SINGLE_CHAT = "5";
    //群聊
    String GROUP_CHAT = "6";
    //系统通知
    String SYS_NOTIFY = "7";
    //系统推送到指定客户端
    String SYS_CLIENT = "8";
}
