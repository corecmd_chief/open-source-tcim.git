package com.corecmd.tcloud.tcimcommon.im.domain;

import lombok.Data;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Data
public abstract class Packet {
    /**
     * 协议版本
     */
    private Byte version = 1;

    /**
     * 指令
     * @return
     */
    public abstract Byte getCommand();
}
