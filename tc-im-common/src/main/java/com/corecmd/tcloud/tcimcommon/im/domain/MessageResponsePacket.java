package com.corecmd.tcloud.tcimcommon.im.domain;

import lombok.Data;

import static com.corecmd.tcloud.tcimcommon.im.common.Command.MESSAGE_RESPONSE;


/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
@Data
public class MessageResponsePacket extends Packet {
    @Override
    public Byte getCommand() {
        return MESSAGE_RESPONSE;
    }
}
