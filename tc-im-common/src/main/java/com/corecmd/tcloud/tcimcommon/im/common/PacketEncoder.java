package com.corecmd.tcloud.tcimcommon.im.common;

import com.corecmd.tcloud.tcimcommon.im.domain.Packet;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class PacketEncoder extends MessageToByteEncoder<Packet> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Packet packet, ByteBuf byteBuf) throws Exception {
        new PacketCodeC().encode(packet);
    }
}
