package com.corecmd.tcloud.tcimcommon.im.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public interface SerializerAlgorithm {
    /**
     * json 序列化标识
     */
    byte JSON = 1;
}
