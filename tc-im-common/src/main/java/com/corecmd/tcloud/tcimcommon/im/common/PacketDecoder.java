package com.corecmd.tcloud.tcimcommon.im.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class PacketDecoder extends ByteToMessageDecoder{
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        list.add(new PacketCodeC().decode(byteBuf));
    }
}
