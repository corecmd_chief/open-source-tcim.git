package com.corecmd.tcloud.tcim.domain;

import java.io.Serializable;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/24
 * @description :
 **/
public class ImLoginInfo implements Serializable {
    private String userId;
    private String imHost;
    private int imPort;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImHost() {
        return imHost;
    }

    public void setImHost(String imHost) {
        this.imHost = imHost;
    }

    public int getImPort() {
        return imPort;
    }

    public void setImPort(int imPort) {
        this.imPort = imPort;
    }
}
