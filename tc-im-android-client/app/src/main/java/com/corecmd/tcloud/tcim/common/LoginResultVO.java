package com.corecmd.tcloud.tcim.common;

import java.io.Serializable;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/28
 * @copyright :
 */
public class LoginResultVO implements Serializable {
    private String userId;
    private String statusCode;
    private String message;
    private String imHost;
    private int imPort;
    private String authToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImHost() {
        return imHost;
    }

    public void setImHost(String imHost) {
        this.imHost = imHost;
    }

    public int getImPort() {
        return imPort;
    }

    public void setImPort(int imPort) {
        this.imPort = imPort;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public String toString() {
        return "LoginResultVO{" +
                "userId='" + userId + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", message='" + message + '\'' +
                ", imHost='" + imHost + '\'' +
                ", imPort=" + imPort +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}
