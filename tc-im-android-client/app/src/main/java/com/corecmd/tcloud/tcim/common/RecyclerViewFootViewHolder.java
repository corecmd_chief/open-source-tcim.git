package com.corecmd.tcloud.tcim.common;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.corecmd.tcloud.tcim.R;


public class RecyclerViewFootViewHolder extends BaseViewHolder {
    private TextView loadMoreTips;
    private ProgressBar loadMoreProgressBar;
    public RecyclerViewFootViewHolder(Context context, @NonNull View itemView) {
        super(context,itemView);
        loadMoreTips = (TextView) itemView.findViewById(R.id.loadMoreTips);
        loadMoreProgressBar = (ProgressBar) itemView.findViewById(R.id.loadMoreProgressBar);
    }

    public TextView getLoadMoreTips() {
        return loadMoreTips;
    }

    public void setLoadMoreTips(TextView loadMoreTips) {
        this.loadMoreTips = loadMoreTips;
    }

    public ProgressBar getLoadMoreProgressBar() {
        return loadMoreProgressBar;
    }

    public void setLoadMoreProgressBar(ProgressBar loadMoreProgressBar) {
        this.loadMoreProgressBar = loadMoreProgressBar;
    }
}
