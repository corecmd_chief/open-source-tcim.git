package com.corecmd.tcloud.tcim.threads;


import android.os.Handler;

import com.corecmd.tcloud.tcim.domain.ImLoginInfo;
import com.corecmd.tcloud.tcim.im.StartupImClient;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 **/
public class StartImClientThread implements Runnable {
    private Handler handler;
    private ImLoginInfo imLoginInfo;
    public StartImClientThread(Handler handler, ImLoginInfo imLoginInfo){
        this.handler = handler;
        this.imLoginInfo = imLoginInfo;
    }
    @Override
    public void run() {
        StartupImClient client = new StartupImClient(this.handler,this.imLoginInfo);
        client.start();
    }
}
