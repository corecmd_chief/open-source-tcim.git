package com.corecmd.tcloud.tcim.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;

import com.corecmd.tcloud.tcim.R;
import com.corecmd.tcloud.tcim.common.Constant;
import com.corecmd.tcloud.tcim.domain.ImLoginInfo;
import com.corecmd.tcloud.tcim.threads.StartImClientThread;
import com.corecmd.tcloud.tcim.utils.AppUtil;

import java.util.List;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/24
 * @description :
 **/
public class ImClientService extends Service {
    private Handler mHandler;
    private Context mContext;
    private String mAppPackageName;
    private static int notifyId = 0;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        ImLoginInfo imLoginInfo = (ImLoginInfo)extras.getSerializable(Constant.IM_LOGIN_INFO_SER_KEY);
        mContext = this;
        mAppPackageName = AppUtil.getAppPackageName(mContext);
        mHandler = new Handler(new Handler.Callback() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean handleMessage(Message msg) {
                int msgType = msg.what;
                Bundle bundle = msg.getData();
                if (msgType == Constant.IM_MESSAGE_PUSH_KEY){
                    //广播消息
                    Intent intent = new Intent(Constant.IM_MESSAGE_BROADCAST_ACTION);
                    intent.putExtra("messageContent",bundle.getString("messageContent"));
                    intent.putExtra("receiverId",bundle.getString("receiverId"));
                    intent.putExtra("senderId",bundle.getString("senderId"));
                    sendBroadcast(intent);
                    //判断当前应用是否在前台，否则推送消息
                    if (!AppUtil.isTopActivity(mContext,mAppPackageName)){
                        System.out.println("应用不在前台，启动通知");
                        // 创建通知(标题、内容、图标)
                        Notification notification = new Notification.Builder(mContext)
                                .setContentTitle("您有一条新消息")
                                .setContentText(bundle.getString("messageContent"))
                                .setSmallIcon(R.mipmap.ic_launcher_round)
                                .build();
                        // 创建通知管理器
                        //NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        // 发送通知
                       // manager.notify(notifyId++, notification);

                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
                        notificationManager.notify(notifyId++, notification);
                    }
                }
                return false;
            }
        });
        //启动客户端连接
        new Thread(new StartImClientThread(this.mHandler,imLoginInfo)).start();
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"服务已停止",Toast.LENGTH_SHORT).show();
    }
}
