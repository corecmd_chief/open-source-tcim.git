package com.corecmd.tcloud.tcim.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/28
 * @copyright :
 */
public class ToastUtil {
    public static void showToaskShort(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
    public static void showToaskLong(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
