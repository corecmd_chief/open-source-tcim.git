package com.corecmd.tcloud.tcim.ui.chatSessions;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.corecmd.tcloud.tcim.common.BaseViewHolder;
import com.corecmd.tcloud.tcim.common.BaseViewHolderGenerator;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/3/3
 * @copyright :
 */
public class ChatMessageMineViewHolderGenerator extends BaseViewHolderGenerator {
    @Override
    public BaseViewHolder generateVieHolder(Context context, RecyclerView.LayoutManager layoutManager, View view) {
        ChatMessageMineViewHolder chatMessageViewHolder = new ChatMessageMineViewHolder(context,layoutManager,view);
        return chatMessageViewHolder;
    }
}
