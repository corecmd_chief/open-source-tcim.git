package com.corecmd.tcloud.tcim.ui.chatSessions;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.corecmd.tcloud.tcim.common.BaseViewHolder;
import com.corecmd.tcloud.tcim.common.BaseViewHolderGenerator;


public class ChatSessionViewHolderGenerator extends BaseViewHolderGenerator {
    @Override
    public BaseViewHolder generateVieHolder(Context context, RecyclerView.LayoutManager layoutManager, View view) {
        ChatSessionViewHolder messageViewHolder = new ChatSessionViewHolder(context,layoutManager,view);
        return messageViewHolder;
    }
}
