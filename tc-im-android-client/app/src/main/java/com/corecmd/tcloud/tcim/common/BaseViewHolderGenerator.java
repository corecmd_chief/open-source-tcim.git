package com.corecmd.tcloud.tcim.common;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseViewHolderGenerator {
    public abstract BaseViewHolder generateVieHolder(Context context, RecyclerView.LayoutManager layoutManager, View view);
}
