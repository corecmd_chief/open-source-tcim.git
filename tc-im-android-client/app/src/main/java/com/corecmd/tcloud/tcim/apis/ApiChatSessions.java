package com.corecmd.tcloud.tcim.apis;

import android.content.SharedPreferences;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcim.common.Constant;
import com.corecmd.tcloud.tcim.configs.ApiConfigs;
import com.corecmd.tcloud.tcim.domain.ChatSessionVO;
import com.corecmd.tcloud.tcim.utils.HttpUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/28
 * @copyright :
 */
public class ApiChatSessions {

    /**
     * 查询会话列表
     * @param pageNum
     * @param pageSize
     * @param searchParam
     * @return
     */
    public static String listChatSessions(int pageNum, int pageSize, String searchParam){

        String result = null;
        try {
            String url = ApiConfigs.CHAT_SESSIONS_API + "/rest/chatSession/list";
            Map<String,String> query = new HashMap<String, String>();
            query.put("pageNum",String.valueOf(pageNum));
            query.put("pageSize",String.valueOf(pageSize));
            query.put("searchParam",String.valueOf(searchParam));
            result = HttpUtil.httpGet(url,query,null);
            System.out.println("查询会话列表结果：" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 查询会话列表
     * @param pageNum
     * @param pageSize
     * @param searchParam
     * @return
     */
    public static String listChatMessages(String chatSessionId,int pageNum, int pageSize, String searchParam){
        String result = null;
        try {
            String url = ApiConfigs.CHAT_SESSIONS_API + "/rest/chatMessages/sayHello";
            Map<String,String> query = new HashMap<String, String>();
            query.put("pageNum",String.valueOf(pageNum));
            query.put("pageSize",String.valueOf(pageSize));
            query.put("searchParam",String.valueOf(searchParam));
            result = HttpUtil.httpGet(url,query,null);
            System.out.println("查询会话消息列表结果：" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        listChatSessions(1,10,"");
    }

}
