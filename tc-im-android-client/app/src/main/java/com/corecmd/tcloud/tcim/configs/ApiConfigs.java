package com.corecmd.tcloud.tcim.configs;

public class ApiConfigs {

    /**
     * API 请求的根路径
     */
    public static final String remote_server_ipaddr = "网关部署服务器IP:17400/v1/im";
    public static final String API_ROOT = "http://" + remote_server_ipaddr;
    public static final String CHAT_GROUPS_API = API_ROOT;
    public static final String CHAT_MESSAGES_API = API_ROOT;
    public static final String CHAT_SESSIONS_API = API_ROOT;
    public static final String FRIENDS_API = API_ROOT;
    public static final String LOGIN_API = API_ROOT;

}
