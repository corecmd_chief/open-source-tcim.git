package com.corecmd.tcloud.tcim.im;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.corecmd.tcloud.tcimcommon.im.domain.MessageRequestPacket;
import com.corecmd.tcloud.tcim.common.Constant;
import com.corecmd.tcloud.tcim.domain.ImLoginInfo;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class ClientMessageRequestHandler extends SimpleChannelInboundHandler<MessageRequestPacket> {
    private Handler mHandler;
    private ImLoginInfo imLoginInfo;
    public ClientMessageRequestHandler(Handler handler, ImLoginInfo imLoginInfo){
        this.mHandler = handler;
        this.imLoginInfo = imLoginInfo;
    }
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, MessageRequestPacket messageRequestPacket) throws Exception {
        System.out.println("客户端收到服务端消息请求:"+messageRequestPacket.getMsgContent());
        Message msg = new Message();
        msg.what = Constant.IM_MESSAGE_PUSH_KEY;
        Bundle dataBundle = new Bundle();
        dataBundle.putString("messageContent",messageRequestPacket.getMsgContent());
        dataBundle.putString("receiverId",messageRequestPacket.getReceiverId());
        dataBundle.putString("senderId",messageRequestPacket.getSenderId());
        msg.setData(dataBundle);
        mHandler.sendMessage(msg);
        channelHandlerContext.flush();
    }
}
