package com.corecmd.tcloud.tcim.common;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

/**
 *  Created by tianshaojiao on 16/5/19.
 */
public class IconFontView extends androidx.appcompat.widget.AppCompatTextView {
    public static final String FONT_PATH = "iconfont.ttf";
    public static Typeface mTypeface;

    public IconFontView(Context context) {
        super(context);
        init(context);
    }

    public IconFontView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public IconFontView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    private void init(Context context){
        if(mTypeface == null){
            mTypeface = Typeface.createFromAsset(context.getAssets(), FONT_PATH);
        }
        setTypeface(mTypeface);
    }
}
