package com.corecmd.tcloud.tcim.configs;

public class TokenConfigs {
    private static String token = null;

    public static void setToken(String token) {
        System.out.println("设置登录Token="+token);
        TokenConfigs.token = token;
    }
    public static String getToken() {
        System.out.println("取得登录Token="+token);
        return token;
    }
}
