package com.corecmd.tcloud.tcim.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 **/

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class UserAgentIntercepter implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        SharedPreferences sp = MyBaseApplication.getmContext().getSharedPreferences(Constant.LOGININFOKEY, Context.MODE_PRIVATE);
        String token = sp.getString(Constant.TOKENKEY, "");
        System.out.println("添加Token:"+token);
        Request.Builder builder =  chain.request().newBuilder();
        if (null != token){
            builder.addHeader("authToken", token);
        }
        Request request = builder.build();
        return chain.proceed(request);
    }
}
