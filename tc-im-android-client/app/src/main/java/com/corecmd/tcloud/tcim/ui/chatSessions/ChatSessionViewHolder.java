package com.corecmd.tcloud.tcim.ui.chatSessions;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcim.R;
import com.corecmd.tcloud.tcim.common.BaseViewHolder;


public class ChatSessionViewHolder extends BaseViewHolder {

    TextView mMessageTitle;

    public ChatSessionViewHolder(Context context, RecyclerView.LayoutManager layoutManager, @NonNull View itemView) {
        super(context, itemView);
        mMessageTitle = (TextView) itemView.findViewById(R.id.message_title);


    }

    @Override
    public void bindView(Object dataInfo) {
        JSONObject currentMessage = (JSONObject)dataInfo;
        String chatSessionTitle = "消息接收人："+currentMessage.getString("receiverId");
        mMessageTitle.setText(chatSessionTitle);
    }
}
