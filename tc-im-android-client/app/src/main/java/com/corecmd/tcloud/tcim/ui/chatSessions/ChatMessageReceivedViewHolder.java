package com.corecmd.tcloud.tcim.ui.chatSessions;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcim.R;
import com.corecmd.tcloud.tcim.common.BaseViewHolder;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/3/3
 * @copyright :
 */
public class ChatMessageReceivedViewHolder extends BaseViewHolder {
    private TextView mMessageReceived;

    public ChatMessageReceivedViewHolder(Context context, RecyclerView.LayoutManager layoutManager, @NonNull View itemView) {
        super(context, itemView);
        mMessageReceived = itemView.findViewById(R.id.messageReceived);

    }

    @Override
    public void bindView(Object dataInfo) {
        JSONObject currentMessage = (JSONObject)dataInfo;
        System.out.println("当前收到的消息："+currentMessage.toJSONString());
        mMessageReceived.setText(currentMessage.getString("messageContent"));
    }
}
