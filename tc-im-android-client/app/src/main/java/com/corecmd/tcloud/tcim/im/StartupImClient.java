package com.corecmd.tcloud.tcim.im;


import android.os.Handler;

import com.corecmd.tcloud.tcimcommon.im.common.PacketDecoder;
import com.corecmd.tcloud.tcimcommon.im.common.PacketEncoder;
import com.corecmd.tcloud.tcim.domain.ImLoginInfo;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 * http://localhost:18900/im/fetchImInfo
 **/
public class StartupImClient {
    private Handler handler;
    private ImLoginInfo imLoginInfo;
    public StartupImClient(Handler handler, ImLoginInfo imLoginInfo){
        this.handler = handler;
        this.imLoginInfo = imLoginInfo;
    }
    public void start(){
        try {
            establishImConn(imLoginInfo.getImHost(),imLoginInfo.getImPort());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void establishImConn(String host,int port) throws Exception{
        System.out.println("im客户端启动："+host);
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        final Handler mhandler = this.handler;
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap
                    // 1.指定线程模型
                    .group(workerGroup)
                    // 2.指定 IO 类型为 NIO
                    .channel(NioSocketChannel.class)
                    // 3.IO 处理逻辑
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new PacketDecoder());
                            ch.pipeline().addLast(new ClientHandler(mhandler,imLoginInfo));
                            ch.pipeline().addLast(new ClientLoginResponseHandler(mhandler,imLoginInfo));
                            ch.pipeline().addLast(new ClientMessageRequestHandler(mhandler,imLoginInfo));
                            ch.pipeline().addLast(new ClientMessageResponseHandler(mhandler,imLoginInfo));
                            ch.pipeline().addLast(new PacketEncoder());
                        }
                    });
            // 4.建立连接
            ChannelFuture future = bootstrap.connect(host, port).sync();
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()){
                        System.out.println("连接成功");
                    } else {
                        System.out.println("连接失败");
                    }
                }
            });
            // 等待服务端监听端口关闭
            future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
        }
    }
}
