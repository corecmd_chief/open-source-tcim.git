package com.corecmd.tcloud.tcim.utils;

import com.corecmd.tcloud.tcim.common.UserAgentIntercepter;

import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;


/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 **/
public class HttpUtil {

    public static String httpGet(String url, Map<String,String> params,Map<String,String> headers) throws Exception{
        String result = null;
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        httpClient.addNetworkInterceptor(new UserAgentIntercepter());
        if (null != params && !params.isEmpty()){
            Iterator<Map.Entry<String,String>> paramEntrysIt = params.entrySet().iterator();
            StringBuilder sb = new StringBuilder();
            while (paramEntrysIt.hasNext()){
                Map.Entry<String,String> entry = paramEntrysIt.next();
                sb.append("&");
                sb.append(entry.getKey());
                sb.append("=");
                sb.append(entry.getValue());
            }
            url += "?"+sb.toString().substring(1);
        }
        Request.Builder builder = new Request.Builder();
        builder.get().url(url);
        if (null != headers && !headers.isEmpty()){
            Iterator<Map.Entry<String,String>> headerEntrysIt = headers.entrySet().iterator();
            while (headerEntrysIt.hasNext()){
                Map.Entry<String,String> headerEntry = headerEntrysIt.next();
                builder.addHeader(headerEntry.getKey(),headerEntry.getValue());
            }
        }
        Request request = builder.build();
        Call call = httpClient.build().newCall(request);
        Response response = call.execute();
        ResponseBody responseBody = response.body();
        if (null != responseBody){
            result = responseBody.string();
        }
        return result;
    }

    public static String httpPost(String url, Map<String,String> params,Map<String,String> headers) throws Exception{
        String result = null;
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        httpClient.addNetworkInterceptor(new UserAgentIntercepter());
        Request.Builder builder = new Request.Builder();
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (null != params && !params.isEmpty()){
            Iterator<Map.Entry<String,String>> paramEntrysIt = params.entrySet().iterator();
            while (paramEntrysIt.hasNext()){
                Map.Entry<String,String> entry = paramEntrysIt.next();
                System.out.println(entry.getKey()+":"+entry.getValue());
                formBodyBuilder.add(entry.getKey(),entry.getValue());
            }
        }
        RequestBody requestBody = formBodyBuilder.build();;
        builder.post(requestBody).url(url);
        if (null != headers && !headers.isEmpty()){
            Iterator<Map.Entry<String,String>> headerEntrysIt = headers.entrySet().iterator();
            while (headerEntrysIt.hasNext()){
                Map.Entry<String,String> headerEntry = headerEntrysIt.next();
                builder.addHeader(headerEntry.getKey(),headerEntry.getValue());
            }
        }
        Request request = builder.build();
        Call call = httpClient.build().newCall(request);
        Response response = call.execute();
        ResponseBody responseBody = response.body();
        if (null != responseBody){
            result = responseBody.string();
        }
        return result;
    }

    public static void main(String[] args) {
        try {
            System.out.println(HttpUtil.httpGet("http://192.168.1.6:18900/login/imLogin",null,null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}