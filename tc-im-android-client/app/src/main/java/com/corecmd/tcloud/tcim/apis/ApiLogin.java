package com.corecmd.tcloud.tcim.apis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.corecmd.tcloud.tcim.common.LoginResultVO;
import com.corecmd.tcloud.tcim.configs.ApiConfigs;
import com.corecmd.tcloud.tcim.utils.HttpUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/2/28
 * @copyright :
 */
public class ApiLogin {
    public static LoginResultVO login(String account, String password){
        LoginResultVO loginResultVO = null;
        Map<String,String> loginParam = new HashMap<String,String>();
        loginParam.put("userId",account);
        loginParam.put("password",password);
        try {
            String httpResult = HttpUtil.httpPost(ApiConfigs.LOGIN_API + "/login/imLogin",loginParam,null);
            JSONObject jsonObject = JSON.parseObject(httpResult);
            System.out.println("登录结果："+httpResult);
            String statusCode = jsonObject.getString("statusCode");
            if (!statusCode.isEmpty() && "200".equals(statusCode)){
                String message = jsonObject.getString("message");
                loginResultVO = new LoginResultVO();
                loginResultVO.setStatusCode(statusCode);
                loginResultVO.setMessage(message);
                loginResultVO.setAuthToken(jsonObject.getString("authToken"));
                loginResultVO.setImHost(jsonObject.getString("imHost"));
                loginResultVO.setImPort(jsonObject.getIntValue("imPort"));
                loginResultVO.setUserId(account);
            } else {
                loginResultVO = new LoginResultVO();
                loginResultVO.setStatusCode("0");
                loginResultVO.setMessage("登录失败");
            }
        } catch (Exception e) {
            loginResultVO = new LoginResultVO();
            loginResultVO.setStatusCode("0");
            loginResultVO.setMessage("登录异常");
            System.out.println("登录异常"+e.getMessage());
        }
        return loginResultVO;
    }

    public static void main(String[] args) {
        login("8","Tian2334227..");
    }
}
