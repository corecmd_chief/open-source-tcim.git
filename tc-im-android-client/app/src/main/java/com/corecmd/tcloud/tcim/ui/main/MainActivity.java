package com.corecmd.tcloud.tcim.ui.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.corecmd.tcloud.tcim.R;
import com.corecmd.tcloud.tcim.common.Constant;
import com.corecmd.tcloud.tcim.common.IconFontView;
import com.corecmd.tcloud.tcim.services.ImClientService;
import com.corecmd.tcloud.tcim.ui.chatSessions.ChatSessionsFragment;
import com.corecmd.tcloud.tcim.ui.contacts.ContactsFragment;
import com.corecmd.tcloud.tcim.ui.discovery.DiscoveryFragment;
import com.corecmd.tcloud.tcim.ui.me.MeFragment;
import com.corecmd.tcloud.tcim.utils.ThreadPoolFactory;
import com.corecmd.tcloud.tcim.utils.ToastUtil;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private LinearLayout mTabChatSessions;
    private LinearLayout mTabContacts;
    private LinearLayout mTabDiscovery;
    private LinearLayout mTabMe;

    private Fragment mFragmentChatSessions;
    private Fragment mFragmentContacts;
    private Fragment mFragmentDiscovery;
    private Fragment mFragmentMe;

    private TextView mTabChatSessionsText;
    private TextView mTabContactsText;
    private TextView mTabDiscoveryText;
    private TextView mTabMeText;

    private IconFontView mTabChatSessionsIcon;
    private IconFontView mTabContactsIcon;
    private IconFontView mTabDiscoveryIcon;
    private IconFontView mTabMeIcon;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        initView();
        initEvents();
        setSelect(R.id.id_tab_chat_sessions);
    }
    private void initEvents() {
        mTabChatSessions.setOnClickListener(this);
        mTabContacts.setOnClickListener(this);
        mTabDiscovery.setOnClickListener(this);
        mTabMe.setOnClickListener(this);
    }
    private void initView() {
        mTabChatSessions = (LinearLayout) findViewById(R.id.id_tab_chat_sessions);
        mTabContacts = (LinearLayout) findViewById(R.id.id_tab_contacts);
        mTabDiscovery = (LinearLayout) findViewById(R.id.id_tab_discovery);
        mTabMe = (LinearLayout) findViewById(R.id.id_tab_me);

        mTabChatSessionsText = (TextView)findViewById(R.id.tab_chat_sessions_text);
        mTabContactsText = (TextView)findViewById(R.id.tab_contacts_text);
        mTabDiscoveryText = (TextView)findViewById(R.id.tab_discovery_text);
        mTabMeText = (TextView)findViewById(R.id.tab_me_text);

        mTabChatSessionsIcon = (IconFontView)findViewById(R.id.tab_chat_sessions_icon);
        mTabContactsIcon = (IconFontView)findViewById(R.id.tab_contacts_icon);
        mTabDiscoveryIcon = (IconFontView)findViewById(R.id.tab_discovery_icon);
        mTabMeIcon = (IconFontView)findViewById(R.id.tab_me_icon);
    }
    /**
     * 重置底部tab的图标为暗色
     */
    private void resetBottomTabImages(){
        int defaultTextColor = R.color.bottom_bar_text_default;
        mTabChatSessionsText.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabContactsText.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabDiscoveryText.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabMeText.setTextColor(this.getResources().getColor(defaultTextColor));

        mTabChatSessionsIcon.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabChatSessionsIcon.setText(this.getResources().getString(R.string.icon_message));

        mTabContactsIcon.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabContactsIcon.setText(this.getResources().getString(R.string.icon_contacts));

        mTabDiscoveryIcon.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabDiscoveryIcon.setText(this.getResources().getString(R.string.icon_discovery));

        mTabMeIcon.setTextColor(this.getResources().getColor(defaultTextColor));
        mTabMeIcon.setText(this.getResources().getString(R.string.icon_me));
    }
    private void setSelect(int bottomTabKeyId){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        hideAllFragments(transaction);
        int selectedColor = R.color.bottom_bar_text_selected;
        //把图标设置为亮色
        //设置内容区域
        switch (bottomTabKeyId){
            case R.id.id_tab_chat_sessions:
                if (mFragmentChatSessions == null){
                    mFragmentChatSessions = new ChatSessionsFragment();
                    transaction.add(R.id.id_app_mainContent,mFragmentChatSessions);
                } else {
                    transaction.show(mFragmentChatSessions);
                }
                mTabChatSessionsText.setTextColor(this.getResources().getColor(selectedColor));
                mTabChatSessionsIcon.setTextColor(this.getResources().getColor(selectedColor));
                mTabChatSessionsIcon.setText(this.getResources().getString(R.string.icon_message_selected));
                break;
            case R.id.id_tab_contacts:
                if (mFragmentContacts == null){
                    mFragmentContacts = new ContactsFragment();
                    transaction.add(R.id.id_app_mainContent,mFragmentContacts);
                } else {
                    transaction.show(mFragmentContacts);
                }
                mTabContactsText.setTextColor(this.getResources().getColor(selectedColor));
                mTabContactsIcon.setTextColor(this.getResources().getColor(selectedColor));
                mTabContactsIcon.setText(this.getResources().getString(R.string.icon_contacts_selected));
                break;
            case R.id.id_tab_discovery:
                if (mFragmentDiscovery == null){
                    mFragmentDiscovery = new DiscoveryFragment();
                    transaction.add(R.id.id_app_mainContent,mFragmentDiscovery);
                } else {
                    transaction.show(mFragmentDiscovery);
                }
                mTabDiscoveryText.setTextColor(this.getResources().getColor(selectedColor));
                mTabDiscoveryIcon.setTextColor(this.getResources().getColor(selectedColor));
                mTabDiscoveryIcon.setText(this.getResources().getString(R.string.icon_discovery_selected));
                break;
            case R.id.id_tab_me:
                if (mFragmentMe == null){
                    mFragmentMe = new MeFragment();
                    transaction.add(R.id.id_app_mainContent,mFragmentMe);
                } else {
                    transaction.show(mFragmentMe);
                }
                mTabMeText.setTextColor(this.getResources().getColor(selectedColor));
                mTabMeIcon.setTextColor(this.getResources().getColor(selectedColor));
                mTabMeIcon.setText(this.getResources().getString(R.string.icon_me_selected));
                break;
        }
        transaction.commit();
    }

    /**
     * 隐藏所有Fragment
     * @param transaction
     */
    private void hideAllFragments(FragmentTransaction transaction) {
        if (mFragmentChatSessions != null){
            transaction.hide(mFragmentChatSessions);
        }
        if (mFragmentContacts != null){
            transaction.hide(mFragmentContacts);
        }
        if (mFragmentDiscovery != null){
            transaction.hide(mFragmentDiscovery);
        }
        if (mFragmentMe != null){
            transaction.hide(mFragmentMe);
        }
    }

    @Override
    public void onClick(View v) {
        resetBottomTabImages();
        int evKeyId = v.getId();
        switch (evKeyId){
            case R.id.id_tab_chat_sessions:
                setSelect(evKeyId);
                break;
            case R.id.id_tab_contacts:
                setSelect(evKeyId);
                break;
            case  R.id.id_tab_discovery:
                ToastUtil.showToaskShort(this,"功能开发中...");
                //setSelect(evKeyId);
                break;
            case R.id.id_tab_me:
                ToastUtil.showToaskShort(this,"功能开发中...");
                //setSelect(evKeyId);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        //关闭线程池
        ThreadPoolFactory.getSendImMsgThreadPool().shutdown();
        Intent stopIntent = new Intent(this, ImClientService.class);
        stopService(stopIntent);
        super.onDestroy();
    }
}
