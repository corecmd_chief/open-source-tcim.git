package com.corecmd.tcloud.tcim.common;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.corecmd.tcloud.tcim.R;

import java.util.List;

public abstract  class RecyclerViewCommonAdapter<T> extends RecyclerView.Adapter<BaseViewHolder>{
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;
    private Context mContext;
    private int mLayoutId;
    private BaseViewHolderGenerator viewHolderGenerator;
    private List<T> mData;
    //上拉加载更多
    public static final int PULLUP_LOAD_MORE = 0;
    //正在加载中
    public static final int LOADING_MORE = 1;
    //没有加载更多 隐藏
    public static final int NO_LOAD_MORE = 2;
    //没有加载更多 隐藏
    public static final int REFRESHING = 3;
    //上拉加载更多状态-默认为0
    private int mLoadMoreStatus = 0;
    RecyclerView.LayoutManager mLayoutManager;
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public RecyclerViewCommonAdapter(Context context, RecyclerView.LayoutManager layoutManager, BaseViewHolderGenerator viewHolderGenerator, int mLayoutId, List<T> data) {
        this.mContext = context;
        this.mData = data;
        this.mLayoutId = mLayoutId;
        this.viewHolderGenerator = viewHolderGenerator;
        this.mLayoutManager = layoutManager;
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(mLayoutId, parent,
                    false);
            BaseViewHolder viewHolder = null;
            try {
                viewHolder = viewHolderGenerator.generateVieHolder(mContext,mLayoutManager,view);
            }catch (Exception e) {
                Log.d("exceptions","取得视频viewholder异常" + e.getMessage());
            }
            assert viewHolder != null;
            return viewHolder;
        }
        else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.loading_foo_loading, parent,
                    false);
            return new RecyclerViewFootViewHolder(mContext,view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (!(holder instanceof RecyclerViewFootViewHolder)){
            convert(holder, mData.get(position));
        } else {
            RecyclerViewFootViewHolder footerViewHolder = (RecyclerViewFootViewHolder) holder;
            System.out.println("更新加载更多视图："+mLoadMoreStatus);
            switch (mLoadMoreStatus) {
                case PULLUP_LOAD_MORE:
                    footerViewHolder.getLoadMoreProgressBar().setVisibility(View.GONE);
                    footerViewHolder.getLoadMoreTips().setVisibility(View.VISIBLE);
                    break;
                case LOADING_MORE:
                    footerViewHolder.getLoadMoreProgressBar().setVisibility(View.VISIBLE);
                    footerViewHolder.getLoadMoreTips().setVisibility(View.VISIBLE);
                    break;
                case NO_LOAD_MORE:
                    footerViewHolder.getLoadMoreProgressBar().setVisibility(View.GONE);
                    footerViewHolder.getLoadMoreTips().setVisibility(View.VISIBLE);
                    break;
                case REFRESHING:
                    //隐藏加载更多
                    footerViewHolder.getLoadMoreProgressBar().setVisibility(View.GONE);
                    footerViewHolder.getLoadMoreTips().setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size()+1;
    }
    public T getItemInfo(int position){
        return mData.size() == 0 ? null:mData.get(position);
    }
    /**
     * 更新加载更多状态
     * @param status
     */
    public void changeMoreStatus(int status){
        mLoadMoreStatus=status;
        notifyDataSetChanged();
    }
    /**
     * 对外提供的方法
     */
    public abstract void convert(final BaseViewHolder holder, T t);

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public List<T> getmData() {
        return mData;
    }

    public void setmData(List<T> mData) {
        this.mData = mData;
    }
}
