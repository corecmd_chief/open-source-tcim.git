package com.corecmd.tcloud.tcim.im;

import android.os.Handler;

import com.corecmd.tcloud.tcimcommon.im.domain.LoginResponsePacket;
import com.corecmd.tcloud.tcim.domain.ImLoginInfo;
import com.corecmd.tcloud.tcim.utils.ImSessionHolder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class ClientLoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket> {
    private Handler mHandler;
    private ImLoginInfo imLoginInfo;
    public ClientLoginResponseHandler(Handler handler, ImLoginInfo imLoginInfo){
        this.mHandler = handler;
        this.imLoginInfo = imLoginInfo;
    }
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, LoginResponsePacket loginResponsePacket) throws Exception {
        System.out.println("客户端收到登陆请求回应："+loginResponsePacket.getMessage());
        if (200 == loginResponsePacket.getStatusCode()){
            ImSessionHolder.put(imLoginInfo.getUserId(),(NioSocketChannel) channelHandlerContext.channel());
        }
        channelHandlerContext.flush();
    }
}
