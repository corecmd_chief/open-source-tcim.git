package com.corecmd.tcloud.tcim.im;

import android.os.Handler;

import com.corecmd.tcloud.tcimcommon.im.domain.MessageResponsePacket;
import com.corecmd.tcloud.tcim.domain.ImLoginInfo;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/20
 * @description :
 **/
public class ClientMessageResponseHandler extends SimpleChannelInboundHandler<MessageResponsePacket> {
    private Handler mHandler;
    private ImLoginInfo imLoginInfo;
    public ClientMessageResponseHandler(Handler handler, ImLoginInfo imLoginInfo){
        this.mHandler = handler;
        this.imLoginInfo = imLoginInfo;
    }
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, MessageResponsePacket messageResponsePacket) throws Exception {
        System.out.println("服务端收到消息发送请求回应");
        channelHandlerContext.flush();
    }
}
