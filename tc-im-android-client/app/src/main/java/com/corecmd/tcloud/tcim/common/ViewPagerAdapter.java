package com.corecmd.tcloud.tcim.common;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * viewpager适配器
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    List<? extends Fragment> fragmentList;
    public ViewPagerAdapter(FragmentManager fm, List<? extends Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

}
