package com.corecmd.tcloud.tcim.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author : tianshaojiao
 * @version : 1.0
 * @apiNote :
 * @date : 2021/3/5
 * @copyright :
 */
public class ThreadPoolFactory {
    private static ExecutorService sendImMsgThreadPool;
    public static ExecutorService getSendImMsgThreadPool(){
        if (null != sendImMsgThreadPool){
            return sendImMsgThreadPool;
        }
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("im-message-push-pool").build();
        sendImMsgThreadPool = new ThreadPoolExecutor(1,5,0L, TimeUnit.MILLISECONDS
                ,new LinkedBlockingDeque<Runnable>(1024),threadFactory,new ThreadPoolExecutor.AbortPolicy());
        return sendImMsgThreadPool;
    }
}
