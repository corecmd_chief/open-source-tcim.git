package com.corecmd.tcloud.tcim.common;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/24
 * @description :
 **/
public class Constant {
    public final static String IM_MESSAGE_BROADCAST_ACTION = "im.message.broadcast";
    public final static String IM_LOGIN_INFO_SER_KEY = "com.corecmd.tcloud.timclient.domain.ImLoginInfo";
    public final static int IM_MESSAGE_PUSH_KEY = 0X1234567;
    public final static String IM_WEB_LOGIN_RESULT_INFO_KEY = "com.corecmd.tcloud.timclient.common.LoginResultVO";
    public static final String LOGININFOKEY = "loginInfo";
    public static final String TOKENKEY = "authToken";
    public static final String IMHOST = "imHost";
    public static final String IMPORT = "imPort";
    public static final String IMUSERID = "imUserId";
    public static final String TCIMPACKAGENAME = "com.corecmd.tcloud.tcim";
}
