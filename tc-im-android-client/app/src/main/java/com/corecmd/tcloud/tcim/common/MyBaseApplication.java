package com.corecmd.tcloud.tcim.common;


import android.app.Application;
import android.os.Handler;

import androidx.multidex.MultiDex;

/**
 * @author : TianShaoJiao
 * @date : 2021/2/23
 * @description :
 **/
public class MyBaseApplication extends Application {
    //获取到主线程的上下文
    private static MyBaseApplication mContext;
    //获取到主线程的handler
    private static Handler mMainThreadHanler;
    //获取到主线程
    private static Thread mMainThread;
    //获取到主线程的id
    private static int mMainThreadId;
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        this.mContext = this;
        this.mMainThreadHanler = new Handler();
        this.mMainThread = Thread.currentThread();
        //获取到调用线程的id
        this.mMainThreadId = android.os.Process.myTid();
    }

    public static MyBaseApplication getmContext() {
        return mContext;
    }

    public static Handler getmMainThreadHanler() {
        return mMainThreadHanler;
    }

    public static Thread getmMainThread() {
        return mMainThread;
    }

    public static int getmMainThreadId() {
        return mMainThreadId;
    }
}