# open-source-tcim

#### 介绍
open-source-tcim，是基于netty的客户端+服务器简单即时通讯系统。服务端基于springboot，前端采用android开发；后端是简单的微服务架构，采用consul作为服务注册中心，服务端和网关服务需要注册到注册中心；前端对服务端的http请求都需要经过网关，登录session采用redis缓存，因此服务端可进行集群部署。

#### 软件架构

![tcim架构图](https://images.gitee.com/uploads/images/2021/0309/135650_e54e49ff_8185763.png "tcim架构图.png")

#### 安装教程


1.  搭建redis缓存服务器，具体搭建教程，可以参考我的博客： [Centos7安装Redis5.0.3步骤](http://www.tianshaojiao.com/?p=118)
1.  搭建mysql数据库服务器，具体搭建教程，可以参考我的博客： [Docker部署Mysql](http://www.tianshaojiao.com/?p=71)
1.  数据库服务器配置后，新建数据库，库名自定义，然后根据tc-im-server/src/main/resources/config/database.sql下的sql，新建数据库表。
1.  搭建consul服务器，下载consul安装包到本地，具体安装方式请自行百度
1.  克隆本仓库代码到本机，[https://gitee.com/corecmd_chief/open-source-tcim.git](https://gitee.com/corecmd_chief/open-source-tcim.git)
1.  使用编辑器，例如idea,打开tc-im-common公用包，依赖下载完毕后，点击maven install将公用包安装到本地maven仓库
1.  使用编辑器，例如idea,打开tcgateway网关项目，依赖下载完毕后，修改配置文件，将application-dev.yml 中的consul IP及端口修改为你自己的服务器配置，然后运行项目
1.  使用编辑器，例如idea，打开tc-im-server项目，依赖下载完毕后，修改配置文件，将application-dev.yml 中的consul IP及端口修改为你自己的服务器配置，redis配置，fastdfs配置（目前还没有更新fastdfs的代码，请稍等），imserver的netty配置；修改数据库配置为你自己的配置。
1.  运行tc-im-server项目，项目启动后，会提示项目正在你配置的im端口监听
1.  访问tc-im-server 接口文档页面，例如： [http://localhost:18900/doc.html](http://localhost:18900/doc.html)，若出现此页面，则说明服务端项目部署成功
 ![接口文档](https://images.gitee.com/uploads/images/2021/0309/110741_23b257e2_8185763.png "屏幕截图.png")
1.  使用androidstudio打开tc-im-android-client客户端项目，待项目构建完毕后，需要修改app/src/main/java/com/corecmd/tcloud/tcim/configs/ApiConfigs.java下的api配置，将api地址修改为你自己的网关地址，例如： [http://192.168.1.4:17400/v1/im](http://localhost:17400/v1/im).注意，若你的服务部署在本机，那么api地址请不要使用127.0.0.1或者localhost，这是由于这两个ip在android中另有用处。可以使用：win+r-->cmd-->ipconfig查看本机ip地址。修改好配置后。运行客户端项目，进入到登录页面，如下所示：![客户端登录页面](https://images.gitee.com/uploads/images/2021/0309/111256_b451d6bc_8185763.png "屏幕截图.png")
1.  登录账号，你需要自行注册，注册地址为您的imserver接口文档页面，如下所示：
   ![tcim用户注册](https://images.gitee.com/uploads/images/2021/0309/111410_5decff6e_8185763.png "屏幕截图.png")

#### 系统演示

![tcim客户端演示](https://images.gitee.com/uploads/images/2021/0309/102026_c3da56d6_8185763.gif "tcim.gif")

#### 后期优化

1. 支持群聊
1. 支持图片、视频、文件的传输
1. 搭建fastdfs分布式文件服务器，具体搭建教程可参考：[使用 FastDFS搭建文件服务器](http://www.tianshaojiao.com/?p=107)
1. 仿微信朋友圈，发布动态功能。
1. 以及视频上传时，自动生成静态图和动态图预览图片
1. 聊天消息转发等

#### 说明

本项目是为了学习netty而搭建，旨在给大家学习netty是提供一个参考。目前仅支持单聊，并且聊天内容仅支持文本。后续还需要优化一下项目，以达到支持图片、视频、文件等内容。至于android客户端的界面，也是本人简单构建的，毕竟我不是专业搞android的，有兴趣的同学可以继续优化一下客户端代码，然后我们一起优化一下它吧。


